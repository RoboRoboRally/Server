﻿using RoboRoboRally.Server.Core.Listeners;
using RoboRoboRally.Server.Core.Lobbies;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.Generic;

namespace RoboRoboRally.Server.Core.Connections
{
    public abstract class BaseConnection : IConnection
    {
        protected HashSet<Type> SupportedTypes = new HashSet<Type>();

        public virtual string ClientName { get; set; }
        public virtual bool CanPlay => SupportedTypes.Contains(typeof(IPlayerClient));
        public virtual Guid ConnectionId { get; set; } = Guid.NewGuid();

        public virtual void SetSupportedTypes(Type[] types)
        {
            foreach (var type in types)
                SupportedTypes.Add(type);
        }

        public abstract T GetClient<T>() where T : class, IClient;

        public virtual bool SupportsClient<T>() where T : class, IClient
        {
            return SupportedTypes.Contains(typeof(T));
        }

        public virtual void Close()
        {
        }

        public virtual void SendMessageToAllClients(Action<IClient> message)
        {
            CheckAndSend<IErrorSpectator>(message);
            CheckAndSend<IGameFlowSpectator>(message);
            CheckAndSend<ILobbySpectator>(message);
            CheckAndSend<IMapElementSpectator>(message);
            CheckAndSend<IMovementSpectator>(message);
            CheckAndSend<IPlayerClient>(message);
            CheckAndSend<IProgrammingCardSpectator>(message);
            CheckAndSend<IUpgradeCardSpectator>(message);
            CheckAndSend<IRobotSpectator>(message);
            //TODO: make generic with use of reflection
            //TODO: maybe don't send duplicates?
        }

        protected void CheckAndSend<T>(Action<IClient> message) where T : class, IClient
        {
            if (SupportsClient<T>())
            {
                message(GetClient<T>());
            }
        }

        public virtual ILobby CreateLobbyListener(IServerLobby lobby)
        {
            var listener = new LobbyListener(this)
            {
                Lobby = lobby
            };
            return listener;
        }

        public virtual IOwnedLobby CreateOwnedLobbyListener(IServerLobby lobby)
        {
            var listener = new LobbyListener(this)
            {
                Lobby = lobby
            };
            return listener;
        }

        public virtual IGame CreateGameListener(IServerGame game)
        {
            var listener = new GameListener(this)
            {
                Game = game
            };
            return listener;
        }

        public virtual void RemovedFromLobby()
        {
        }
    }
}