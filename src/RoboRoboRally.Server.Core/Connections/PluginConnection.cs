﻿using Common.Logging;
using RoboRoboRally.Server.Core.Listeners;
using RoboRoboRally.Server.Core.Lobbies;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.Generic;

namespace RoboRoboRally.Server.Core.Connections
{
    internal class PluginConnection : BaseConnection
    {
        private IDictionary<Type, IClient> clients = new Dictionary<Type, IClient>();
        private IPlugin plugin;
        private ILog log = LogManager.GetLogger<PluginConnection>();

        public override string ClientName { get => plugin.PluginInfo.Name; set { } }

        public PluginConnection(IServerLobby lobby, IPlugin plugin)
        {
            this.plugin = plugin;
            var listener = new LobbyListener(this);
            listener.Lobby = lobby;
            lobby.AddConnection(this);
            try
            {
                var clients = plugin.JoinLobby(listener);
                foreach (var (type, client) in clients)
                {
                    if (!type.IsInstanceOfType(client))
                    {
                        log.Info($"Plugin {plugin.PluginInfo.Name} v{plugin.PluginInfo.Version} returned mismatching client of claimed type {type.FullName}");
                        throw new InvalidOperationException($"Plugin returned a client for {type.FullName} that did not implement said interface");
                    }
                }
                this.clients = clients;

                log.Info($"Plugin {plugin.PluginInfo.Name} v{plugin.PluginInfo.Version} added to lobby {lobby.GetLobbyId()}");
            }
            catch (Exception e)
            {
                log.Warn($"Plugin {plugin.PluginInfo.Name} v{plugin.PluginInfo.Version} threw an exception while being created:", e);
                lobby.RemoveConnection(this, this);
                throw;
            }
        }

        public override bool SupportsClient<T>()
        {
            log.Debug($"Checking whether plugin {plugin.PluginInfo.Name} v{plugin.PluginInfo.Version} supports {typeof(T).Name}");
            var result = clients.ContainsKey(typeof(T));
            log.Debug($"Done");
            return result;
        }

        public override T GetClient<T>()
        {
            return clients[typeof(T)] as T;
        }
    }
}