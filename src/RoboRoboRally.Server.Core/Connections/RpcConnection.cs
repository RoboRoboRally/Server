﻿using RoboRoboRally.Communications;
using RoboRoboRally.Server.Core.Listeners;
using RoboRoboRally.Server.Core.Lobbies;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.Generic;

namespace RoboRoboRally.Server.Core.Connections
{
    internal class RpcConnection : BaseConnection
    {
        private IServer ServerListener { get; set; }

        private ILobbyListener LobbyListener { get; set; }

        private IGameListener GameListener { get; set; }

        private IRpcConnection rpcConnection;

        private Dictionary<Type, IClient> clients = new Dictionary<Type, IClient>();

        public RpcConnection(IRpcConnection rpcConnection, LobbyManager lobbyManager)
        {
            this.rpcConnection = rpcConnection;
            LobbyListener = new LobbyListener(this);
            ServerListener = new ServerListener(this, lobbyManager);
            GameListener = new GameListener(this);

            rpcConnection.Register<IServer>(ServerListener);
            rpcConnection.Register<ILobby>(LobbyListener);
            rpcConnection.Register<IGame>(GameListener);

            rpcConnection.OnClose += (_, __) =>
            {
                lock (clients)
                    clients.Clear();
                LobbyListener.Lobby?.RemoveConnection(this, this);
                ServerListener = null;
                LobbyListener = null;
                GameListener = null;                
            };
        }

        public override T GetClient<T>()
        {
            Type key = typeof(T);
            if (clients[key] == null)
            {
                clients[key] = rpcConnection.GetRemote<T>();
            }
            return clients[key] as T;
        }

        public override bool SupportsClient<T>()
        {
            return clients.ContainsKey(typeof(T));
        }

        public override void SetSupportedTypes(Type[] types)
        {
            foreach (var type in types)
                clients.Add(type, null);
        }

        public override void Close()
        {
            rpcConnection?.Dispose();
        }

        public override ILobby CreateLobbyListener(IServerLobby lobby)
        {
            LobbyListener.Lobby = lobby;
            return null;
        }

        public override IOwnedLobby CreateOwnedLobbyListener(IServerLobby lobby)
        {
            LobbyListener.Lobby = lobby;
            return null;
        }

        public override IGame CreateGameListener(IServerGame game)
        {
            GameListener.Game = game;
            return null;
        }
    }
}