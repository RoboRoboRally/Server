﻿using RoboRoboRally.Server.Core.Lobbies;
using RoboRoboRally.Server.Interfaces;

namespace RoboRoboRally.Server.Core.Connections
{
    public class LocalConnection : BaseConnection
    {
        private IGame Game;// { get; private set; }
        private IOwnedLobby OwnedLobby; //{ get; private set; }

        public override T GetClient<T>()
        {
            return null;
        }

        public override IGame CreateGameListener(IServerGame game)
        {
            Game = base.CreateGameListener(game);
            return Game;
        }

        public override IOwnedLobby CreateOwnedLobbyListener(IServerLobby lobby)
        {
            OwnedLobby = base.CreateOwnedLobbyListener(lobby);
            return OwnedLobby;
        }

        public IGame GetGame() => Game;
    }
}