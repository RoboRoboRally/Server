﻿using RoboRoboRally.Server.Core.Settings;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using ModelGame = RoboRoboRally.Model.Game;

namespace RoboRoboRally.Server.Core.Lobbies
{
    partial class Lobby : IServerLobby
    {
        public string LobbyId { get; private set; }
        public bool GameIsRunning { get; private set; }
        public IConnection LobbyOwner { get; private set; }

        private LobbyConnections Connections = new LobbyConnections();
        private LobbyManager lobbyManager;
        private IPluginManager pluginManager;
        private ModelGame ModelGame;
        private object _lock = new object();
        private IConfig config;
        private bool closed = false;
        private IServerGame game;

        public Lobby(string lobbyId,
            IConnection lobbyCreator,
            LobbyManager lobbyManager,
            IConfig config,
            IPluginManager pluginManager)
        {
            LobbyId = lobbyId;
            LobbyOwner = lobbyCreator;
            this.lobbyManager = lobbyManager;
            Connections.AddConnection(LobbyOwner);
            AddRobots();
            this.config = config;
            ModelGame = new ModelGame(config.AbsoluteGameDataFolder);
            this.pluginManager = pluginManager;

            Connections.OnRobotFreed += robot =>
            {
                if (GameIsRunning)
                    ModelGame.RemovePlayer(robot.Name);
            };
            Connections.OnPluginRemoved += (plugin, connection) =>
            {
                pluginManager.RemovePluginFromLobby(plugin, connection.CreateLobbyListener(this));
            };
        }

        private void AddRobots()
        {
            var robots = new[]
            {
                new RobotInfo {Name = "Hulk X90", Color = "Red", Order = 0},
                new RobotInfo {Name = "Spin bot", Color = "Blue", Order = 1},
                new RobotInfo {Name = "Twonky", Color = "Orange", Order = 2},
                new RobotInfo {Name = "Hammer bot", Color = "Purple", Order = 3},
                new RobotInfo {Name = "Smash bot", Color = "Yellow", Order = 4},
                new RobotInfo {Name = "Zoom bot", Color = "Green", Order = 5},
            };

            Connections.SetupRobots(robots);
        }

        private RobotInfo GetRobot(string robotName)
        {
            return Connections.GetRobot(robotName);
        }

        public void AddConnection(IConnection connection)
        {
            CheckClosed();
            Connections.AddConnection(connection);      

            if (GameIsRunning)
                Task.Run(() => SendGameStarted(connection));
        }

        private void CheckClosed()
        {
            lock(_lock)
                if (closed)
                    throw new InvalidOperationException("Lobby closed");
        }

        public void RemoveConnection(IConnection removingConnection, IConnection removedConnection)
        {
            lock (_lock)
            {
                CheckClosed();
                bool removingOneself = removingConnection == removedConnection;
                bool ownerKickingSomeone = removingConnection == LobbyOwner;
                if (!removingOneself && !ownerKickingSomeone)
                    return;

                if (removedConnection == LobbyOwner)
                {
                    CloseLobby(removingConnection);
                    return;
                }
            }

            Connections.RemoveConnection(removedConnection);
        }

        public RobotInfo[] GetAvailableRobots()
        {
            CheckClosed();
            return Connections
                .GetRobotConnections()
                .Where(pair => pair.Value == null)
                .Select(pair => pair.Key)
                .ToArray();
        }

        public void ChangeRoleToSpectator(IConnection connection)
        {
            lock (_lock)
            {
                CheckClosed();
                Connections.CheckConnectionInLobby(connection);
            }
                var robot = Connections
                    .GetRobotConnections()
                    .Where(pair => pair.Value == connection)
                    .Select(pair => pair.Key).FirstOrDefault();

            Connections.FreeRobot(robot);
        }

        public void SelectRobot(IConnection connection, RobotInfo robotInfo)
        {
            CheckClosed();
            Connections.AssignRobot(connection, robotInfo);

            if (GameIsRunning)
            {
                lock(_lock)
                    ModelGame.AddPlayer(robotInfo.Name);
            }
        }

        public ClientRole GetRole(IConnection connection)
        {
            CheckClosed();
            Connections.CheckConnectionInLobby(connection);

            if (Connections
                .GetRobotConnections()
                .Where(pair => pair.Value == connection)
                .Any())
                return ClientRole.Player;
            else
                return ClientRole.Spectator;
        }

        public void StartGame(IConnection connection, GameSettings settings)
        {
            lock (_lock)
            {
                if (GameIsRunning)
                    throw new InvalidOperationException("Game is already running");
                CheckClosed();

                if (CanStartGame(connection))
                {
                    var set = new SettingsLoader().GetSettings(config, settings);

                    var playedRobots = Connections
                        .GetRobotConnections()
                        .Where(pair => pair.Value != null)
                        .Select(pair => pair.Key.Name)
                        .ToArray();

                    Connections.ResetRobotsWhoWon();
                    game = new Game(ModelGame, settings, Connections);
                    var notifiers = new GameEventNotifier(Connections, game, new ModelCardLoader(config));
                    notifiers.GameIsRunningChanged += isRunning => GameIsRunning = isRunning;

                    ModelGame.StartGame(set, playedRobots, this, notifiers);
                    GameIsRunning = true;
                }
            }
        }

        private void SendGameStarted(IConnection connection)
        {
            var listener = connection.CreateGameListener(game);
            connection.SendMessageToAllClients(c => c.GameStarted(listener));
        }

        private bool CanStartGame(IConnection connection)
        {
            return connection == LobbyOwner;
        }

        private IConnection GetConnection(string robotName)
        {
            return Connections.GetConnection(robotName);
        }

        public void CloseLobby(IConnection connection)
        {
            lock (_lock)
            {
                closed = true;
                lobbyManager.CloseLobby(LobbyId);
                ModelGame.EndGame();

                Connections.SendMessage<ILobbySpectator>(s => s.LobbyClosing());

                foreach (var con in Connections.GetConnections())
                {
                    con.RemovedFromLobby();
                    Connections.SendMessage<ILobbySpectator>(s => s.ClientLeft(con.ClientName));
                }

                Connections.SendMessage<ILobbySpectator>(s => s.LobbyClosed());
            }
        }

        public RobotInfo[] GetAllRobots()
        {
            CheckClosed();
            return Connections
                .GetRobotConnections()
                .Select(p => p.Key)
                .ToArray();
        }

        public PluginInfo[] GetAvailablePlugins()
        {
            CheckClosed();
            return pluginManager
                .GetAvailablePlugins(this)
                .Select(id => (PluginInfo)id)
                .ToArray();
            
        }

        public PluginInfo[] AddPlugin(IConnection connection, PluginInfo id)
        {
            lock (_lock)
            {
                CheckClosed();
                if (connection != LobbyOwner)
                    throw new InvalidOperationException("Only lobby owner can add plugins");

                var pluginConnection = pluginManager.AddPluginToLobby(id, this);
                Connections.AddPlugin(pluginConnection, id);
                return GetAvailablePlugins();
            }
        }

        public string GetLobbyId() => LobbyId;

        public void RemoveConnection(IConnection removingConnection, ConnectionInfo removedConnection)
        {
            CheckClosed();
            var removed = Connections.GetConnections()
                .Where(c => c.ConnectionId.ToString() == removedConnection.ConnectionId)
                .FirstOrDefault();
            if (removed == null)
                return;

            
            RemoveConnection(removingConnection, removed);
        }

        public ConnectionInfo[] GetConnectionInfos(IConnection connection)
        {
            CheckClosed();
            return Connections.GetConnections()
                .Select(c => new ConnectionInfo
                {
                    ConnectionId = c.ConnectionId.ToString(),
                    ConnectionName = c.ClientName,
                    SelectedRobot = Connections
                        .GetRobotConnections()
                        .Where(p => p.Value == c)
                        .Select(p => p.Key)
                        .FirstOrDefault()
                })
                .ToArray();
        }

        public InstantiatedPluginInfo[] GetAddedPlugins(IConnection connection)
        {
            CheckClosed();

            return Connections.GetAddedPlugins();            
        }

        public void FreeRobot(IConnection connection, RobotInfo robot)
        {
            lock (_lock)
            {
                CheckClosed();

                if (LobbyOwner != connection)
                    throw new InvalidOperationException("Only lobby owner can free robots.");
            }
            Connections.FreeRobot(robot);            
        }

        public void RemovePlugin(IConnection connection, InstantiatedPluginInfo plugin)
        {
            lock (_lock)
            {
                CheckClosed();

                if (LobbyOwner != connection)
                    throw new InvalidOperationException("Only lobby owner can free robots.");
            }
            var removed = Connections.GetConnections().Where(c => c.ConnectionId.ToString() == plugin.InstanceId).FirstOrDefault();

            if (removed != null)
                RemoveConnection(connection, removed);
        }
    }
}
