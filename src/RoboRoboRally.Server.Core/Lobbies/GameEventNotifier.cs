﻿using RoboRoboRally.Model;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Server.Core.Settings;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace RoboRoboRally.Server.Core.Lobbies
{
    [ExcludeFromCodeCoverage]
    internal class GameEventNotifier : IGameEventNotifier
    {
        private LobbyConnections connections;
        private int turnNumber = 0;
        private IServerGame game;
        private ICardLoader cardLoader;

        public event Action<bool> GameIsRunningChanged;

        public GameEventNotifier(LobbyConnections connections, IServerGame game, ICardLoader cardLoader)
        {
            this.connections = connections;
            this.game = game;
            this.cardLoader = cardLoader;
        }

        private void SendMessage<T>(Action<T> message)
            where T : class, IClient
            => connections.SendMessage(message);

        private void SendMessageToOnePlayer<T>(string robotName, Action<T> message)
            where T: class, IClient
        {
            var connection = connections.GetConnection(robotName);
            if (connection == null || !connection.SupportsClient<T>())
                return;

            var client = connection.GetClient<T>();
            message(client);
        }

        private RobotInfo GetRobot(string robotName) => connections.GetRobot(robotName);

        void IGameEventNotifier.ActivationPhasePriorityDetermined(string[] robotsByPriority)
        {
            SendMessage<IGameFlowSpectator>(s => s.RobotPriorityDetermined(robotsByPriority.Select(n => new RobotInfo { Name = n }).ToArray()));
        }

        void IGameEventNotifier.BlueBeltsActivated()
        {
            SendMessage<IMapElementSpectator>(s => s.BlueBeltsActivated());
        }

        void IGameEventNotifier.CheckpointsActivated()
        {
            SendMessage<IMapElementSpectator>(s => s.CheckpointsActivated());
        }

        void IGameEventNotifier.EnergySpacesActivated()
        {
            SendMessage<IMapElementSpectator>(s => s.EnergySpacesActivated());
        }

        void IGameEventNotifier.FatalRunningGameError(string errorMessage)
        {
            SendMessage<IErrorSpectator>(s => s.FatalRunningGameError(errorMessage));
        }

        void IGameEventNotifier.FatalUnexpectedError(string errorMessage)
        {
            SendMessage<IErrorSpectator>(s => s.FatalUnexpectedError(errorMessage));
        }

        void IGameEventNotifier.GameOver()
        {
            SendMessage<IGameFlowSpectator>(s => s.GameOver());
            GameIsRunningChanged?.Invoke(false);
        }

        void IGameEventNotifier.GameIsEnding()
        {
            SendMessage<IGameFlowSpectator>(s => s.GameIsEnding());
        }

        void IGameEventNotifier.GamePaused()
        {
            SendMessage<IGameFlowSpectator>(s => s.GamePaused());
        }

        void IGameEventNotifier.GameResumed()
        {
            SendMessage<IGameFlowSpectator>(s => s.GameResumed());
        }

        void IGameEventNotifier.GreenBeltsActivated()
        {
            SendMessage<IMapElementSpectator>(s => s.GreenBeltsActivated());
        }

        void IGameEventNotifier.MapElementsActivationBegan()
        {
            SendMessage<IMapElementSpectator>(s => s.MapElementActivationStarted());
        }

        void IGameEventNotifier.NewTurnBegan()
        {
            ++turnNumber;
            SendMessage<IGameFlowSpectator>(s => s.TurnStarted(turnNumber));
        }

        void IGameEventNotifier.PhaseEnded(string phaseName)
        {
            SendMessage<IGameFlowSpectator>(s => s.PhaseEnded(phaseName));
        }

        void IGameEventNotifier.PhaseStarted(string phaseName)
        {
            SendMessage<IGameFlowSpectator>(s => s.PhaseStarted(phaseName));
        }

        void IGameEventNotifier.ProgrammingCardPlayed(string robotName, string cardName)
        {
            SendMessage<IProgrammingCardSpectator>(s => s.CardBeingPlayed(GetRobot(robotName), cardName));
        }

        void IGameEventNotifier.ProgrammingCardRevealed(string robotName, int registerNumber, string cardName)
        {
            SendMessage<IProgrammingCardSpectator>(s => s.CardRevealed(new RevealedProgrammingCard
            {
                Robot = GetRobot(robotName),
                Register = registerNumber,
                RevealedCard = cardName
            }));
        }

        void IGameEventNotifier.PushPanelsActivated()
        {
            SendMessage<IMapElementSpectator>(s => s.PushPanelsActivated());
        }

        void IGameEventNotifier.RegisterActivationBegan(int registerNumber)
        {
            SendMessage<IProgrammingCardSpectator>(s => s.RegisterActivated(registerNumber));
        }

        void IGameEventNotifier.RobotBurnedCard(string robotName, string cardName)
        {
            SendMessage<IProgrammingCardSpectator>(s => s.CardBurned(GetRobot(robotName), cardName));
        }

        void IGameEventNotifier.RobotEnergyChanged(string robotName, int newEnergyAmount, int oldEnergyAmount)
        {
            SendMessage<IRobotSpectator>(s => s.RobotEnergyChanged(GetRobot(robotName), oldEnergyAmount, newEnergyAmount));
            SendMessageToOnePlayer<IPlayerClient>(robotName, c => c.BatteryCountChanged(newEnergyAmount));
        }

        void IGameEventNotifier.RobotFinishedGame(string robotName)
        {
            SendMessage<IGameFlowSpectator>(s => s.RobotFinishedGame(GetRobot(robotName)));
            connections.RobotWon(GetRobot(robotName));
            ((IGameEventNotifier)this).RobotLeftGame(robotName);
        }

        void IGameEventNotifier.RobotGotPushed(string robotName, MapCoordinates oldPosition, MapCoordinates newPosition)
        {
            SendMessage<IMovementSpectator>(s => s.RobotPushed(GetRobot(robotName), oldPosition, newPosition));
        }

        void IGameEventNotifier.RobotIsRebooting(string robotName, MapCoordinates rebootPosition, Direction direction)
        {
            SendMessage<IMovementSpectator>(s => s.RobotRebooting(GetRobot(robotName), rebootPosition, direction));
        }

        void IGameEventNotifier.RobotJoinedGame(string robotName, Direction facingDir, MapCoordinates position, int energy, int lastCheckpoint, string[] upgradeCards)
        {
            var cards = cardLoader.LoadUpgradeCards(upgradeCards);            

            var robotState = new RobotState
            {
                Energy = energy,
                LastPassedCheckpoint = lastCheckpoint,
                Robot = GetRobot(robotName),
                RobotPosition = new OrientedMapCoordinates
                {
                    Coordinates = position,
                    Direction = facingDir
                },
                UpgradeCards = cards
                    .GroupBy(c => c.UpgradeCardType)
                    .ToDictionary(
                        g => g.Key,
                        g => g.Select(c => new UpgradeCardInfo { CardId = c.Name, CardType = c.UpgradeCardType, Cost = c.EnergyCost })
                            .ToArray())
            };

            SendMessage<IRobotSpectator>(s => s.RobotActivated(robotState));
            SendMessageToOnePlayer<IPlayerClient>(robotName, c => c.StateInitialisation(robotState));

            //left here for backward compatibility with plotter plugin
            SendMessage<IMovementSpectator>(s => s.RobotAdded(GetRobot(robotName), position, facingDir));
        }

        void IGameEventNotifier.RobotLasersActivated(string robotName)
        {
            SendMessage<IRobotSpectator>(s => s.RobotLasersActivated(GetRobot(robotName)));
        }

        void IGameEventNotifier.RobotLeftGame(string robotName)
        {
            SendMessage<IRobotSpectator>(s => s.RobotDeactivated(GetRobot(robotName)));

            //left here for backward compatibility with plotter plugin
            SendMessage<IMovementSpectator>(s => s.RobotRemoved(GetRobot(robotName)));
        }

        void IGameEventNotifier.RobotMoved(string robotName, MapCoordinates oldPosition, MapCoordinates newPosition)
        {
            SendMessage<IMovementSpectator>(s => s.RobotMoved(GetRobot(robotName), newPosition));
        }

        void IGameEventNotifier.RobotRotated(string robotName, RotationType rotationType)
        {
            SendMessage<IMovementSpectator>(s => s.RobotRotated(GetRobot(robotName), rotationType));
        }

        void IGameEventNotifier.RobotSufferedDamage(string robotName, Model.Util.Damage damage)
        {
            SendMessage<IProgrammingCardSpectator>(s => s.DamageCardsTaken(
                GetRobot(robotName),
                new Interfaces.Damage { DamageCards = damage.DamageCards.Select(c => c.Name).ToArray() }
                ));
        }

        void IGameEventNotifier.RobotTeleported(string robotName, MapCoordinates oldPosition, MapCoordinates newPosition, Direction newFacingDirection)
        {
            SendMessage<IMovementSpectator>(s => s.RobotTeleported(GetRobot(robotName), newPosition, newFacingDirection));
        }

        void IGameEventNotifier.RobotVisitedCheckpoint(string robotName, int checkpointNumber)
        {
            SendMessage<IGameFlowSpectator>(s => s.RobotVisitedCheckpoint(GetRobot(robotName), checkpointNumber));
        }

        void IGameEventNotifier.RotatingGearsActivated()
        {
            SendMessage<IMapElementSpectator>(s => s.RotatingGearsActivated());
        }

        void IGameEventNotifier.TurnEnded()
        {
            SendMessage<IGameFlowSpectator>(s => s.TurnEnded(turnNumber));
        }

        void IGameEventNotifier.WallLasersActivated()
        {
            SendMessage<IMapElementSpectator>(s => s.WallLasersActivated());
        }

        void IGameEventNotifier.RegisterCardReplaced(string robotName, int registerNumber, string oldCardName, string newCardName)
        {
            SendMessage<IProgrammingCardSpectator>(s => s.RevealedCardReplaced(
                GetRobot(robotName),
                registerNumber,
                new CardInfo { CardId = oldCardName },
                new CardInfo { CardId = newCardName }));
        }

        void IGameEventNotifier.GameStarted(MapSize mapSize)
        {
            game.GetMapInfo().MapSize = mapSize;
            SendGameStarted();
        }

        private void SendGameStarted()
        {
            foreach (var connection in connections.GetConnections())
                SendGameStarted(connection);
        }

        private void SendGameStarted(IConnection connection)
        {
            var listener = connection.CreateGameListener(game);
            connection.SendMessageToAllClients(c => c.GameStarted(listener));
        }

        void IGameEventNotifier.UpgradePhasePriorityDetermined(string[] robotsByPriority)
        {
            ((IGameEventNotifier)this).ActivationPhasePriorityDetermined(robotsByPriority);
        }

        void IGameEventNotifier.RobotIsBuying(string robotName, string[] upgradeCards)
        {
            // is sent when buy command is issued;
        }

        void IGameEventNotifier.UpgradePhaseSkipped()
        {
            // is sent when buy command is issued;
        }

        void IGameEventNotifier.UpgradeCardObtained(string robotName, string cardName)
        {
            // is sent when buy command is issued;
        }

        void IGameEventNotifier.UpgradeCardDiscarded(string robotName, string cardName)
        {
            // is sent when buy command is issued; //TODO: check
        }

        void IGameEventNotifier.UpgradeCardUsed(string robotName, string cardName)
        {
            SendMessage<IUpgradeCardSpectator>(s => s.UpgradeCardUsed(GetRobot(robotName), new CardInfo { CardId = cardName }));
        }

        void IGameEventNotifier.UpgradeCardUsed(string robotName, string cardName, UpgradeCardChoice takenChoice)
        {
            SendMessage<IUpgradeCardSpectator>(s => s.UpgradeCardUsed(GetRobot(robotName), new CardInfo { CardId = cardName }));
        }
    }
}