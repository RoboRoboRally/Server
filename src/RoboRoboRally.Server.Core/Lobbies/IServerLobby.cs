﻿using RoboRoboRally.Server.Interfaces;

namespace RoboRoboRally.Server.Core.Lobbies
{
    public interface IServerLobby : ILobbyInfo
    {
        bool GameIsRunning { get; }
        IConnection LobbyOwner { get; }

        void AddConnection(IConnection connection);

        void ChangeRoleToSpectator(IConnection connection);

        void CloseLobby(IConnection connection);

        ClientRole GetRole(IConnection connection);

        void RemoveConnection(IConnection removingConnection, IConnection removedConnection);

        void RemoveConnection(IConnection removingConnection, ConnectionInfo removedConnection);

        void SelectRobot(IConnection connection, RobotInfo robotInfo);

        void StartGame(IConnection connection, GameSettings settings);

        PluginInfo[] GetAvailablePlugins();

        PluginInfo[] AddPlugin(IConnection connection, PluginInfo id);

        ConnectionInfo[] GetConnectionInfos(IConnection connection);

        InstantiatedPluginInfo[] GetAddedPlugins(IConnection connection);

        void FreeRobot(IConnection connection, RobotInfo robot);

        void RemovePlugin(IConnection connection, InstantiatedPluginInfo plugin);
    }
}