﻿using Common.Logging;
using RoboRoboRally.Model;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RoboRoboRally.Server.Core.Lobbies
{
    partial class Lobby : IInputHandler
    {
        private ILog log = LogManager.GetLogger<Lobby>();

        ProgrammingCard IInputHandler.ChooseCard(string robotName, ProgrammingCard[] cardsToChoose)
        {
            var offer = new ProgramOffer
            {
                Amount = 1,
                CardsToChoose = cardsToChoose,
                RobotName = robotName
            };

            var task = GetProgram(offer, default(CancellationToken));

            WaitForResults(new[] { offer }, new List<Task<ProgramChoice>> { task }).Wait();

            if (task.Status == TaskStatus.RanToCompletion)
                return task.Result.ChosenCards[0];
            else
                return cardsToChoose[0];
        }

        ProgramChoice[] IInputHandler.GetPrograms(ProgramOffer[] robotProgramOffers)
        {
            Connections.SendMessage<IProgrammingCardSpectator>(s => s.CardsDealt());

            List<Task<ProgramChoice>> tasks = new List<Task<ProgramChoice>>();
            CancellationTokenSource cts = new CancellationTokenSource();

            foreach (var offer in robotProgramOffers)
            {
                tasks.Add(GetProgram(offer, cts.Token));
            }

            try
            {
                WaitForResults(robotProgramOffers, tasks).Wait();
            }
            catch(Exception e)
            {
                log.Warn("An exception was thrown while getting programs: ", e);
            }

            ProgramChoice[] result = new ProgramChoice[robotProgramOffers.Length];
            for (int i = 0; i < robotProgramOffers.Length; ++i)
            {
                if (tasks[i].Status == TaskStatus.RanToCompletion)
                    result[i] = tasks[i].Result;
                else
                    result[i] = new ProgramChoice
                    {
                        RobotName = robotProgramOffers[i].RobotName,
                        EmptyProgram = true
                    };
            }

            Connections.SendMessage<IProgrammingCardSpectator>(s => s.CardSelectionOver());
            return result;
        }

        private async Task WaitForResults(ProgramOffer[] robotProgramOffers, List<Task<ProgramChoice>> tasks)
        {
            if (config.ProgrammingTimeout.TotalSeconds == 0)
            {
                await Task.WhenAll(tasks.ToArray());
                return;
            }
            
            var waitAll = Task.WhenAll(tasks.ToArray());
            var waitTimeout = Task.Delay(config.ProgrammingTimeout + config.NetworkLatencyAcceptedDelay);

            Task completed = await Task.WhenAny(waitAll, waitTimeout);

            for (int i = 0; i < robotProgramOffers.Length; ++i)
            {
                var connection = GetConnection(robotProgramOffers[i].RobotName);
                var player = connection?.GetClient<IPlayerClient>();
                if (player != null)
                {
                    try
                    {
                        await Task.Run(() => player.ProgrammingCardTimeout());
                    }
                    catch(Exception e)
                    {
                        log.Warn("Exception thrown by the connection: ", e);
                    }
                }
            }            
        }

        private async Task<ProgramChoice> GetProgram(ProgramOffer offer, CancellationToken cancellationToken)
        {
            var connection = GetConnection(offer.RobotName);
            if (connection == null) // the player disconnected meanwhile
            {
                return new ProgramChoice
                {
                    RobotName = offer.RobotName,
                    EmptyProgram = true
                };
            }

            CardInfo[] cards = offer.CardsToChoose.Select(card => new CardInfo { CardId = card.Name }).ToArray();
            CardInfo[] info;
            var timeout = config.ProgrammingTimeout;
            try
            {
                var client = connection.GetClient<IPlayerClient>();
                var task = Task.Run(() => client.ChooseProgrammingCards(offer.Amount, cards, timeout), cancellationToken);
                info = await task;
                await Connections.SendMessageAsync<IProgrammingCardSpectator>(s => s.CardsSelected(GetRobot(offer.RobotName), info.Length));
            }
            catch (Exception e)
            {
                log.Info($"An error occured while trying to get the program from {offer.RobotName}", e);

                await Connections.SendMessageAsync<IProgrammingCardSpectator>(s => s.CardsNotSelectedOnTime(GetRobot(offer.RobotName)));
                return new ProgramChoice
                {
                    RobotName = offer.RobotName,
                    EmptyProgram = true
                };
            }

            List<ProgrammingCard> cardsLeft = new List<ProgrammingCard>(offer.CardsToChoose);
            List<ProgrammingCard> chosenCards = new List<ProgrammingCard>();

            foreach (var cardInfo in info)
            {
                var card = cardsLeft.Where(c => c.Name == cardInfo.CardId).First();
                chosenCards.Add(card);
                cardsLeft.Remove(card);
            }

            return new ProgramChoice
            {
                RobotName = offer.RobotName,
                ChosenCards = chosenCards.ToArray(),
                EmptyProgram = false
            };
        }

        bool IInputHandler.ShouldGameContinue()
        {
            return true;
        }

        public Model.PurchaseResponse OfferUpgradeCardPurchase(Model.PurchaseOffer offer)
        {
            var sentOffer = CreateOffer(offer);

            Connections.SendMessage<IUpgradeCardSpectator>(s => s.CardsOfferedInShop(GetRobot(offer.RobotName), sentOffer));
            var connection = GetConnection(offer.RobotName);
            if (connection == null)
                return NothingBought(offer.RobotName);

            var client = connection.GetClient<IPlayerClient>();
            var timeout = config.UpgradeCardBuyTimeout;
            var task = client.BuyUpgradeCard(sentOffer, timeout);

            bool success;
            try
            {
                success = task.Wait(timeout + config.NetworkLatencyAcceptedDelay);
            }
            catch(Exception) // client disconnected
            {
                success = false;
            }

            if (success)
            {
                var result = task.Result;
                if (result.SelectedCard == null)
                {
                    Connections.SendMessage<IUpgradeCardSpectator>(s => s.NoCardBought(GetRobot(offer.RobotName)));
                }
                else
                {
                    Connections.SendMessage<IUpgradeCardSpectator>(s => s.CardBought(GetRobot(offer.RobotName), result));
                }

                return new Model.PurchaseResponse
                {
                    PurchasedCard = result.SelectedCard == null ? null : offer.OfferedCards.Where(c => c.Name == result.SelectedCard.CardId).FirstOrDefault(),
                    DiscardedCard = result.DiscardedCard == null ? null : offer.OwnedCards.Where(c => c.Name == result.DiscardedCard.CardId).FirstOrDefault()
                };
            }
            else
                return NothingBought(offer.RobotName);
        }

        private Interfaces.PurchaseOffer CreateOffer(Model.PurchaseOffer offer)
        {
            Func<UpgradeCard, UpgradeCardInfo> convertUpgradeCard = c => new UpgradeCardInfo
            {
                CardId = c.Name,
                CardType = c.UpgradeCardType,
                Cost = c.EnergyCost,
                Choices = null
            };

            return new Interfaces.PurchaseOffer
            {
                Energy = offer.EnergyAmount,
                OfferedCards = offer.OfferedCards.Select(convertUpgradeCard).ToArray(),
                OwnedCardLimit = offer.UpgradeCardsLimits,
                OwnedCards = offer
                    .OwnedCards
                    .Select(convertUpgradeCard)
                    .GroupBy(c => c.CardType)
                    .Select(g => new KeyValuePair<UpgradeCardType, UpgradeCardInfo[]>(g.Key, g.ToArray()))
                    .ToDictionary(p => p.Key, p => p.Value)
            };
        }

        private Model.PurchaseResponse NothingBought(string robotName)
        {
            Connections.SendMessage<IUpgradeCardSpectator>(s => s.NoCardBought(GetRobot(robotName)));
            return new Model.PurchaseResponse
            {
                PurchasedCard = null,
                DiscardedCard = null
            };
        }

        public UpgradeCardUseInfo OfferChoicelessUpgradeCard(string robotName, string upgradeCardName)
        {
            var robot = GetRobot(robotName);
            Connections.SendMessage<IUpgradeCardSpectator>(s => s.UpgradeCardOffered(robot, new CardInfo { CardId = upgradeCardName }));

            var connection = GetConnection(robotName);
            if (connection == null)
                return CardNotUsed(robot);

            var timeout = config.UpgradeCardUseTimeout;

            var task = connection.GetClient<IPlayerClient>().OfferChoicelessUpgradeCard(new CardInfo
            {
                CardId = upgradeCardName
            }, timeout);

            bool success;
            try
            {
                success = task.Wait(timeout + config.NetworkLatencyAcceptedDelay);
            }
            catch(Exception e)
            {
                log.Info("Connection threw an exception, possibly disconnect: ", e);
                success = false;
            }

            if (!success)
                return CardNotUsed(robot);

            var cardUsed = task.Result;
            if (!cardUsed)
                Connections.SendMessage<IUpgradeCardSpectator>(s => s.UpgradeCardNotUsed(robot));

            return new UpgradeCardUseInfo
            {
                UseCard = cardUsed
            };
        }

        private UpgradeCardUseInfo CardNotUsed(RobotInfo robot)
        {
            Connections.SendMessage<IUpgradeCardSpectator>(s => s.UpgradeCardNotUsed(robot));
            return new UpgradeCardUseInfo { UseCard = false };
        }

        public ChoiceUpgradeCardUseInfo OfferChoiceUpgradeCard(string robotName, string upgradeCardName, UpgradeCardChoice[] choices)
        {
            var robot = GetRobot(robotName);
            var upgradeCard = new CardInfo { CardId = upgradeCardName };
            Connections.SendMessage<IUpgradeCardSpectator>(s => s.UpgradeCardOffered(robot, upgradeCard));

            var connection = GetConnection(robotName);
            if (connection == null)
                return ChoiceCardNotUsed(robot);

            var timeout = config.UpgradeCardUseTimeout;
            var task = connection.GetClient<IPlayerClient>().OfferUpgradeCardWithChoices(new UpgradeCardInfo
            {
                CardId = upgradeCardName,
                Choices = choices.Select(c => new CardInfo { CardId = c.ChoiceCardName }).ToArray()
            }, timeout);

            bool success;
            try
            {
                success = task.Wait(timeout + config.NetworkLatencyAcceptedDelay);
            }
            catch(Exception e)
            {
                log.Info("Connection threw an exception, possible disconnect: ", e);
                success = false;
            }

            if (!success)
                return ChoiceCardNotUsed(robot);

            var result = task.Result;

            if (result?.CardId == null)
                return ChoiceCardNotUsed(robot);
                        
            return new ChoiceUpgradeCardUseInfo
            {
                UseCard = true,
                ChosenChoice = new UpgradeCardChoice
                {
                    ChoiceCardName = result.CardId
                }
            };
        }

        private ChoiceUpgradeCardUseInfo ChoiceCardNotUsed(RobotInfo robot)
        {
            Connections.SendMessage<IUpgradeCardSpectator>(s => s.UpgradeCardNotUsed(robot));
            return new ChoiceUpgradeCardUseInfo
            {
                UseCard = false
            };
        }

        public void OfferAsyncChoiceUpgradeCard(string robotName, string upgradeCardName, UpgradeCardChoice[] choices)
        {
            var connection = GetConnection(robotName);
            if (connection == null)
                return;

            var task = connection.GetClient<IPlayerClient>().CardAvailableForPlay(new UpgradeCardInfo
            {
                CardId = upgradeCardName,
                Choices = choices.Select(c => new CardInfo { CardId = c.ChoiceCardName }).ToArray()
            });

            try
            {
                task.Wait(config.AsyncCardOfferProcessTimeout);
            }
            catch(Exception e)
            {
                log.Info("Exception thrown by connection, possibly disconnect: ", e);
            }

            return;
        }

        public void OfferAsyncChoicelessUpgradeCard(string robotName, string upgradeCardName)
        {
            OfferAsyncChoiceUpgradeCard(robotName, upgradeCardName, new UpgradeCardChoice[0]);
        }

        public void EndAsyncUpgradeCardOffer(string robotName, string upgradeCardName)
        {
            var connection = GetConnection(robotName);
            if (connection == null)
                return;

            var task = connection.GetClient<IPlayerClient>().CardNoLongerAvailableForPlay(new CardInfo
            {
                CardId = upgradeCardName
            });

            try
            {
                task.Wait(config.AsyncCardOfferProcessTimeout);
            }
            catch(Exception e)
            {
                log.Info("Exception thrown by connection, possibly disconnect: ", e);
            }

            return;
        }
    }
}