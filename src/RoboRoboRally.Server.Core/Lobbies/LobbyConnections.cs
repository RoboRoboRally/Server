﻿using Common.Logging;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoboRoboRally.Server.Core.Lobbies
{
    internal class LobbyConnections
    {
        public event Action<RobotInfo> OnRobotFreed;

        public event Action<PluginInfo, IConnection> OnPluginRemoved;

        private HashSet<IConnection> connectionsInLobby = new HashSet<IConnection>();
        private Dictionary<RobotInfo, IConnection> selectedRobots = new Dictionary<RobotInfo, IConnection>();
        private Dictionary<IConnection, PluginInfo> InstantiatedPlugins = new Dictionary<IConnection, PluginInfo>();
        private List<RobotInfo> robotsWhoAlreadyWon = new List<RobotInfo>(); // we need to keep this info because model will throw exceptions if we add / remove robots that have already won

        private object _lock = new object();

        private ILog log = LogManager.GetLogger<LobbyConnections>();

        public void SetupRobots(IEnumerable<RobotInfo> robots)
        {
            lock (_lock)
            {
                foreach (var robot in robots)
                    selectedRobots.Add(robot, null);
            }
        }

        public void AddConnection(IConnection connection)
        {
            lock (_lock)
            {
                connectionsInLobby.Add(connection);
                SendMessage<ILobbySpectator>(s => s.ClientJoined(connection.ClientName));
            }
        }

        public void RemoveConnection(IConnection connection)
        {
            lock (_lock)
            {
                if (!connectionsInLobby.Contains(connection))
                    return;

                foreach (var pair in selectedRobots.Where(p => p.Value == connection))
                {
                    FreeRobot(pair.Key);
                    break;
                }

                if (InstantiatedPlugins.ContainsKey(connection))
                {
                    OnPluginRemoved?.Invoke(InstantiatedPlugins[connection], connection);
                    InstantiatedPlugins.Remove(connection);
                }

                SendMessage<ILobbySpectator>(s => s.ClientLeft(connection.ClientName));
                connectionsInLobby.Remove(connection);
            }
        }

        public void FreeRobot(RobotInfo robot)
        {
            lock (_lock)
            {
                if (robot is null || !selectedRobots.ContainsKey(robot))
                    return;

                SendMessage<ILobbySpectator>(s => s.RobotFreed(selectedRobots[robot].ClientName, robot));
                selectedRobots[robot] = null;

				if (robotsWhoAlreadyWon.Contains(robot))
					return;

                OnRobotFreed?.Invoke(robot);
            }
        }

        public void ResetRobotsWhoWon()
        {
            robotsWhoAlreadyWon.Clear();
        }

        public void RobotWon(RobotInfo robot)
        {
            robotsWhoAlreadyWon.Add(robot);
        }

        public void SendMessage<T>(Action<T> message)
            where T : class, IClient
        {
            Task messageTask;
            lock (_lock)
            {
                messageTask = connectionsInLobby.SendMessageAsync(message);
            }

            try
            {
                messageTask.Wait();
            }
            catch(Exception e)
            {
                log.Error("Send message threw an exception: ", e);
            }
        }

        public Task SendMessageAsync<T>(Action<T> message)
            where T : class, IClient
        {
            lock (_lock)
                return connectionsInLobby.SendMessageAsync(message);
        }

        public IEnumerable<KeyValuePair<RobotInfo, IConnection>> GetRobotConnections()
        {
            lock (_lock)
            {
                foreach (var pair in selectedRobots)
                    yield return pair;
            }
        }

        public IEnumerable<IConnection> GetConnections()
        {
            lock (_lock)
            {
                return connectionsInLobby.AsEnumerable();
            }
        }

        public RobotInfo GetRobot(string robotName)
        {
            lock (_lock)
            {
                return selectedRobots
                .Keys
                .Where(r => r.Name == robotName)
                .FirstOrDefault();
            }
        }

        public RobotInfo GetRobot(IConnection connection)
        {
            lock (_lock)
            {
                return selectedRobots
                    .Where(p => p.Value == connection)
                    .Select(p => p.Key)
                    .FirstOrDefault();
            }
        }

        public void CheckConnectionInLobby(IConnection connection)
        {
            lock (_lock)
            {
                if (!connectionsInLobby.Contains(connection))
                    throw new InvalidOperationException("connection not joined in lobby.");
            }
        }

        public RobotInfo[] GetAvailableRobots()
        {
            return selectedRobots
               .Where(pair => pair.Value == null)
               .Select(pair => pair.Key)
               .Except(robotsWhoAlreadyWon)
               .ToArray();
        }

        public void AssignRobot(IConnection connection, RobotInfo robot)
        {
            lock (_lock)
            {
                CheckConnectionInLobby(connection);

                if (!selectedRobots.ContainsKey(robot))
                    throw new InvalidOperationException("chosen robot does not exist");

                if (selectedRobots[robot] != null)
                    throw new InvalidOperationException("Robot already used by someone else");

                if (robotsWhoAlreadyWon.Contains(robot))
                    throw new InvalidOperationException("Robot already won");

                selectedRobots[robot] = connection;
                SendMessage<ILobbySpectator>(s => s.RobotPicked(connection.ClientName, robot));
            }
        }

        public IConnection GetConnection(string robotName)
        {
            lock (_lock)
            {
                return selectedRobots
                .Where(pair => pair.Key.Name == robotName)
                .Select(pair => pair.Value)
                .FirstOrDefault();
            }
        }

        public void AddPlugin(IConnection pluginConnection, PluginInfo id)
        {
            lock (_lock)
            {
                InstantiatedPlugins.Add(pluginConnection, id);
            }
        }

        public InstantiatedPluginInfo[] GetAddedPlugins()
        {
            lock (_lock)
            {
                return InstantiatedPlugins.Select(p => new InstantiatedPluginInfo
                {
                    Plugin = p.Value,
                    InstanceId = p.Key.ConnectionId.ToString()
                })
                .ToArray();
            }
        }
    }
}
