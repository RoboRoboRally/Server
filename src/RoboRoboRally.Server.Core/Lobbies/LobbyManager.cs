﻿using System;
using System.Collections.Generic;

namespace RoboRoboRally.Server.Core.Lobbies
{
    internal class LobbyManager
    {
        private Dictionary<string, IServerLobby> lobbies = new Dictionary<string, IServerLobby>();
        private IConfig config;
        private IPluginManager pluginManager;

        public LobbyManager(IConfig config)
            : this(config, new PluginManager(config.PluginFolders))
        {
        }

        public LobbyManager(IConfig config, IPluginManager pluginManager)
        {
            this.config = config;
            this.pluginManager = pluginManager;
        }

        public IServerLobby CreateLobby(IConnection creator)
        {
            string lobbyId = Guid.NewGuid().ToString();
            return CreateLobby(lobbyId, creator);
        }

        public IServerLobby CreateLobby(string lobbyName, IConnection creator)
        {
            lock (lobbies)
            {
                lobbyName = lobbyName ?? Guid.NewGuid().ToString();

                if (lobbies.ContainsKey(lobbyName))
                    throw new InvalidOperationException("Lobby already exists");
                Lobby result = new Lobby(lobbyName, creator, this, config, pluginManager);
                lobbies.Add(lobbyName, result);
                return result;
            }
        }

        public IServerLobby FindLobby(string lobbyId)
        {
            lock (lobbies)
            {
                if (lobbies.TryGetValue(lobbyId, out IServerLobby lobby))
                    return lobby;
                else
                    return null;
            }
        }

        public void CloseLobby(string lobbyId)
        {
            lock (lobbies)
            {
                lobbies.Remove(lobbyId);
            }
        }
		
		public void CloseAllLobies()
		{
			lock(lobbies)
			{
				foreach(var lobby in lobbies)
				{
					lobby.Value.CloseLobby(null);
				}
				lobbies.Clear();
			}
		}
    }
}
