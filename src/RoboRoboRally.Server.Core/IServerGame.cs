﻿using RoboRoboRally.Server.Interfaces;

namespace RoboRoboRally.Server.Core
{
    public interface IServerGame
    {
        void Pause(IConnection connection);

        void Resume(IConnection connection);

        void Stop(IConnection connection);

        MapInfo GetMapInfo();

        void PlayAsyncCard(IConnection connection, CardInfo playedCard, CardInfo usedChoice);
    }
}