﻿using RoboRoboRally.Server.Core.Lobbies;
using RoboRoboRally.Server.Interfaces;
using System.Collections.Generic;

namespace RoboRoboRally.Server.Core
{
    internal interface IPluginManager
    {
        IEnumerable<PluginId> GetAvailablePlugins(ILobbyInfo lobby);

        IConnection AddPluginToLobby(PluginId info, IServerLobby lobby);

        void RemovePluginFromLobby(PluginId info, ILobby lobby);
    }
}