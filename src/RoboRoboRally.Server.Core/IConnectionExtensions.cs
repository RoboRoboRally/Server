﻿using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RoboRoboRally.Server.Core
{
    internal static class IConnectionExtensions
    {
        public static async Task SendMessageAsync<T>(this IEnumerable<IConnection> connections, Action<T> message)
            where T : class, IClient
        {
            List<Task> tasks = new List<Task>();

            foreach (var connection in connections)
            {
                tasks.Add(Task.Run(() =>
                {
                    try
                    {
                        if (connection.SupportsClient<T>())
                            message(connection.GetClient<T>());
                    }
                    catch (Exception)
                    { }
                }));
            }

            await Task.WhenAll(tasks.ToArray());
        }
    }
}