﻿using RoboRoboRally.Server.Interfaces;

namespace RoboRoboRally.Server.Core
{
    public interface IConnectionServer
    {
        void Start();

        void Close();

        IServer AddLocalConnection(IConnection connection);
    }
}