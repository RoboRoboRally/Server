﻿using Common.Logging;
using RoboRoboRally.Server.Core.Connections;
using RoboRoboRally.Server.Core.Lobbies;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace RoboRoboRally.Server.Core
{
    internal class PluginManager : IPluginManager
    {
        private Dictionary<PluginId, IPlugin> loadedPlugins = new Dictionary<PluginId, IPlugin>();
        private ILog log = LogManager.GetLogger<PluginManager>();

        public PluginManager(IEnumerable<string> pluginFolders)
        {
            log.Info("Loading plugins from:");
            foreach (var folder in pluginFolders)
                log.Info(folder);

            LoadAssemblies(pluginFolders);

            log.Debug($"Loading plugins - matching type {typeof(IPlugin).AssemblyQualifiedName}");
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
                LoadPluginsFromAssembly(assembly);
        }

        public IEnumerable<PluginId> GetAvailablePlugins(ILobbyInfo lobby)
        {
            foreach (var (info, plugin) in loadedPlugins)
            {
                bool success = false;
                try
                {
                    success = plugin.CanJoinLobby(lobby);
                }
                catch (Exception e)
                {
                    log.Error("Plugin threw an exception in CanJoinLobby: ", e);
                }
                if (success)
                    yield return info;
            }
        }

        public IConnection AddPluginToLobby(PluginId info, IServerLobby lobby)
        {
            if (!loadedPlugins.TryGetValue(info, out IPlugin plugin))
                throw new InvalidOperationException("plugin does not exist");

            try
            {
                return new PluginConnection(lobby, plugin);
            }
            catch (Exception e)
            {
                log.Warn($"Error while trying to add plugin {info.Name} v{info.Version} to lobby.", e);
            }
            return null;
        }

        public void RemovePluginFromLobby(PluginId info, ILobby lobby)
        {
            if (!loadedPlugins.TryGetValue(info, out IPlugin plugin))
                return;

            try
            {
                plugin.LeaveLobby(lobby);
            }
            catch (Exception e)
            {
                log.Warn($"Plugin threw an exception when leaving the lobby:", e);
            }
        }

        private void LoadAssemblies(IEnumerable<string> pluginFolders)
        {
            foreach (var file in GetAllAvailableLibraries(pluginFolders))
            {
                try
                {
                    Assembly.LoadFrom(file);
                }
                catch (Exception e) // it's possible the .dll is NOT a .NET assembly, not easily checkable otherwise
                {
                    log.Info($"Failed to load {file}", e);
                }
            }
        }

        private IEnumerable<string> GetAllAvailableLibraries(IEnumerable<string> pluginFolders)
        {
            Queue<string> folders = new Queue<string>(pluginFolders);

            while (folders.Count > 0)
            {
                var folder = folders.Dequeue();
                if (!Directory.Exists(folder))
                    continue;

                foreach (var subFolder in Directory.EnumerateDirectories(folder))
                    folders.Enqueue(subFolder);

                foreach (var file in Directory.EnumerateFiles(folder))
                {
                    if (file.EndsWith(".dll"))
                        yield return file;
                }
            }
        }

        private void LoadPluginsFromAssembly(Assembly assembly)
        {
            try
            {
                log.Debug($"Loading plugins from {assembly.FullName}");
                var pluginTypes = assembly
                .GetExportedTypes()
                .Where(t => IsIPlugin(t));

                foreach (var type in pluginTypes)
                {
                    try
                    {
                        var constructor = type.GetConstructor(new Type[0]);
                        IPlugin plugin = (IPlugin)constructor.Invoke(new object[0]);
                        plugin.Initialize();

                        loadedPlugins.Add(plugin.PluginInfo, plugin);
                        log.Info($"Found plugin {type.FullName} in {assembly.FullName}");
                    }
                    catch (Exception e) //the plugins can throw exceptions when constructed - should not propagate to the rest of the system
                    {
                        log.Error($"Could not instantiate a plugin {type.FullName}", e);
                    }
                }

                log.Debug($"Finished loading plugins from {assembly.FullName}");
            }
            catch (Exception e)
            {
                log.Error($"Error loading classes from {assembly.FullName}", e);
            }
        }

        private bool IsIPlugin(Type t)
        {
            try
            {
                log.Debug($"Checking whether type {t.AssemblyQualifiedName} is a valid plugin...");
                if (t.IsAbstract)
                {
                    log.Debug("Type detected as abstract, check finished.");
                    return false;
                }
                log.Debug("Type is not abstract, proceeding with next check");

                if (t.IsInterface)
                {
                    log.Debug("Type detected as interface, check finished");
                    return false;
                }

                log.Debug($"Detected these implemented interfaces: {string.Join(", ", t.GetInterfaces().Select(i => i.AssemblyQualifiedName))}");

                if (t.GetInterfaces().Contains(typeof(IPlugin)))
                {
                    log.Debug("plugin detected");
                    return true;
                }
                else
                {
                    log.Debug($"type {t.AssemblyQualifiedName} is not a plugin");
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    public static class KeyValuePairDeconstructors //not present in .NET standard 2.0, only .NET core
    {
        public static void Deconstruct<TKey, TValue>(this KeyValuePair<TKey, TValue> pair, out TKey key, out TValue value)
        {
            key = pair.Key;
            value = pair.Value;
        }
    }
}