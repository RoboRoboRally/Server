﻿using Common.Logging;
using Common.Logging.Configuration;
using Common.Logging.Log4Net;

namespace RoboRoboRally.Server.Core
{
    public static class Logging
    {
        static Logging()
        {
            var options = new NameValueCollection();
            options.Add("configType", "INLINE");
            LogManager.Adapter = new Log4NetLoggerFactoryAdapter(options);
        }

        public static void Setup()
        {
            // just here to use the static constructor - only has to be run once
        }
    }
}