﻿using RoboRoboRally.Server.Interfaces;

namespace RoboRoboRally.Server.Core
{
    internal class PluginId
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string Version { get; private set; }

        public static implicit operator PluginInfo(PluginId pluginId)
        {
            return new PluginInfo
            {
                Name = pluginId.Name,
                Version = pluginId.Version,
                Description = pluginId.Description
            };
        }

        public static implicit operator PluginId(PluginInfo pluginInfo)
        {
            return new PluginId
            {
                Name = pluginInfo.Name,
                Version = pluginInfo.Version,
                Description = pluginInfo.Description
            };
        }

        public static bool operator ==(PluginId first, PluginId second)
        {
            return first?.Equals(second) ?? false;
        }

        public static bool operator !=(PluginId first, PluginId second)
        {
            return !(first == second);
        }

        public override bool Equals(object obj)
        {
            PluginId other = obj as PluginId;
            if (other is null)
                return false;

            return Name == other.Name && Version == other.Version && Description == other.Description;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() + Version.GetHashCode() + Description.GetHashCode();
        }
    }
}