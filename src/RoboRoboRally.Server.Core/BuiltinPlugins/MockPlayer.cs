﻿using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoboRoboRally.Server.Core.BuiltinPlugins
{
    public class MockPlayer : IPlugin
    {
        internal class PlayerLogic : IPlayerClient
        {
            void IClient.GameStarted(IGame game)
            {
            }

#pragma warning disable CS1998

            async Task IPlayerClient.BatteryCountChanged(int newBatteryCount)
            {
            }

            async Task<CardInfo[]> IPlayerClient.ChooseProgrammingCards(int count, CardInfo[] cards, TimeSpan timeout)
            {
                if (cards.Length < count)
                    throw new InvalidOperationException("not enough cards to choose from");

                return cards.Take(count).ToArray();
            }
            
            async Task IPlayerClient.ProgrammingCardTimeout()
            {
            }

            async Task<PurchaseResponse> IPlayerClient.BuyUpgradeCard(PurchaseOffer offer, TimeSpan timeout)
            {
                return PurchaseResponse.NothingBought;
            }

            async Task<bool> IPlayerClient.OfferChoicelessUpgradeCard(CardInfo card, TimeSpan timeout)
            {
                return false;
            }

            async Task<CardInfo> IPlayerClient.OfferUpgradeCardWithChoices(UpgradeCardInfo card, TimeSpan timeout)
            {
                return null;
            }

            async Task IPlayerClient.CardAvailableForPlay(UpgradeCardInfo card)
            {
            }

            async Task IPlayerClient.CardNoLongerAvailableForPlay(CardInfo card)
            {
            }

            async Task IPlayerClient.StateInitialisation(RobotState robotState)
            {                
            }

#pragma warning restore CS1998
        }

        public PluginInfo PluginInfo => new PluginInfo
        {
            Name = "AI - take first",
            Version = "1.0",
            Description = "Chooses the first 5 cards that are offered to him"
        };

        public bool CanJoinLobby(ILobbyInfo lobby)
        {
            return lobby.GetAvailableRobots().Length > 0;
        }

        public void Initialize()
        {
        }

        public IDictionary<Type, IClient> JoinLobby(ILobby lobby)
        {
            var robots = lobby.GetAvailableRobots();
            if (robots.Length == 0)
                throw new Exception("Cannot join, no robots available");

            lobby.ChangeMyRoleToPlayer(robots[0]);

            return new Dictionary<Type, IClient>
            {
                {typeof(IPlayerClient), new PlayerLogic() }
            };
        }

        public void LeaveLobby(ILobby lobby)
        {
            lobby.ChangeMyRoleToSpectator();
        }
    }
}