﻿using System;
using System.Collections.Generic;

namespace RoboRoboRally.Server.Core
{
    public interface IConfig
    {
        string AbsoluteGameDataFolder { get; }
        IEnumerable<string> PluginFolders { get; }
        TimeSpan ProgrammingTimeout { get; }
        TimeSpan NetworkLatencyAcceptedDelay { get; }
        TimeSpan UpgradeCardBuyTimeout { get; }
        TimeSpan UpgradeCardUseTimeout { get; }
        TimeSpan AsyncCardOfferProcessTimeout { get; }
    }
}