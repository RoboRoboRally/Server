﻿using RoboRoboRally.Server.Core.Lobbies;
using RoboRoboRally.Server.Interfaces;
using System;

namespace RoboRoboRally.Server.Core.Listeners
{
    partial class ServerListener : IServer
    {
        private IConnection connection;
        private LobbyManager lobbyManager;

        public ServerListener(IConnection connection, LobbyManager lobbyManager)
        {
            this.connection = connection;
            this.lobbyManager = lobbyManager;
        }

        public void Connect(string clientName, params Type[] supportedClients)
        {
            connection.ClientName = clientName;
            connection.SetSupportedTypes(supportedClients);
        }

        public IOwnedLobby CreateLobby(string lobbyId = null)
        {
            var lobby = lobbyManager.CreateLobby(lobbyId, connection);

            return connection.CreateOwnedLobbyListener(lobby);
        }

        public ILobby JoinLobby(string lobbyId)
        {
            var lobby = lobbyManager.FindLobby(lobbyId);
            if (lobby == null)
                throw new LobbyNotFoundException();
            lobby.AddConnection(connection);

            return connection.CreateLobbyListener(lobby);
        }
    }
}