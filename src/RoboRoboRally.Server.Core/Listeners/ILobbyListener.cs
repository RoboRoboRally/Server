﻿using RoboRoboRally.Server.Core.Lobbies;
using RoboRoboRally.Server.Interfaces;

namespace RoboRoboRally.Server.Core.Listeners
{
    internal interface ILobbyListener : IOwnedLobby
    {
        IServerLobby Lobby { get; set; }
    }
}