﻿using RoboRoboRally.Server.Interfaces;

namespace RoboRoboRally.Server.Core.Listeners
{
    internal class GameListener : IGameListener
    {
        private IConnection connection;
        public IServerGame Game { get; set; }

        public GameListener(IConnection connection)
        {
            this.connection = connection;
        }

        public void Pause()
        {
            Game.Pause(connection);
        }

        public void Resume()
        {
            Game.Resume(connection);
        }

        public void StopGame()
        {
            Game.Stop(connection);
        }

        public MapInfo GetMapInfo() => Game.GetMapInfo();

        public void PlayAsyncCard(CardInfo playedCard, CardInfo usedChoice = null)
        {
            Game.PlayAsyncCard(connection, playedCard, usedChoice);
        }
    }
}