﻿using RoboRoboRally.Server.Interfaces;

namespace RoboRoboRally.Server.Core.Listeners
{
    internal interface IGameListener : IGame
    {
        IServerGame Game { get; set; }
    }
}