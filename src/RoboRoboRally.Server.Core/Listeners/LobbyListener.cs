﻿using RoboRoboRally.Server.Core.Lobbies;
using RoboRoboRally.Server.Interfaces;

namespace RoboRoboRally.Server.Core.Listeners
{
    internal class LobbyListener : ILobbyListener
    {
        private IConnection connectionData;

        public IServerLobby Lobby { get; set; }

        public LobbyListener(IConnection connectionData)
        {
            this.connectionData = connectionData;
        }

        public string GetLobbyId() => Lobby.GetLobbyId();

        public void ChangeMyRoleToPlayer(RobotInfo chosenRobot)
        {
            Lobby.SelectRobot(connectionData, chosenRobot);
        }

        public void ChangeMyRoleToSpectator()
        {
            Lobby.ChangeRoleToSpectator(connectionData);
        }

        public RobotInfo[] GetAvailableRobots()
        {
            return Lobby.GetAvailableRobots();
        }

        public ClientRole GetMyRole()
        {
            return Lobby.GetRole(connectionData);
        }

        public void LeaveLobby()
        {
            Lobby.RemoveConnection(connectionData, connectionData);
        }

        public void StartGame(GameSettings settings)
        {
            Lobby.StartGame(connectionData, settings);
        }

        public PluginInfo[] GetAvailablePlugins()
        {
            return Lobby.GetAvailablePlugins();
        }

        public PluginInfo[] AddPlugin(PluginInfo plugin)
        {
            return Lobby.AddPlugin(connectionData, plugin);
        }

        public RobotInfo[] GetAllRobots()
        {
            return Lobby.GetAllRobots();
        }

        public void RemoveConnection(ConnectionInfo connection)
        {
            Lobby.RemoveConnection(this.connectionData, connection);
        }

        public ConnectionInfo[] GetAllConnections()
        {
            return Lobby.GetConnectionInfos(connectionData);
        }

        public InstantiatedPluginInfo[] GetAddedPlugins()
        {
            return Lobby.GetAddedPlugins(connectionData);
        }

        public void FreeRobot(RobotInfo robot)
        {
            Lobby.FreeRobot(connectionData, robot);
        }

        public void RemovePlugin(InstantiatedPluginInfo plugin)
        {
            Lobby.RemovePlugin(connectionData, plugin);
        }
    }
}