﻿using Common.Logging;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Server.Core.Lobbies;
using RoboRoboRally.Server.Interfaces;
using ModelGame = RoboRoboRally.Model.Game;

namespace RoboRoboRally.Server.Core
{
    internal class Game : IServerGame
    {
        private ILog log = LogManager.GetLogger<Game>();

        private ModelGame game;
        private GameSettings settings;
        private MapInfo info;
        private LobbyConnections connections;
        

        public Game(ModelGame game, GameSettings settings, LobbyConnections connections)
        {
            this.game = game;
            this.settings = settings;
            info = new MapInfo
            {
                Settings = settings.MapSettings
            };
            this.connections = connections;
        }

        public MapInfo GetMapInfo()
        {
            return info;
        }

        public void Pause(IConnection connection)
        {
            game.PauseGame();
        }

        public void Resume(IConnection connection)
        {
            game.ResumeGame();
        }

        public void Stop(IConnection connection)
        {
            game.EndGame();
        }

        public void PlayAsyncCard(IConnection connection, CardInfo playedCard, CardInfo usedChoice)
        {
            log.Debug($"In PlayAsyncCard, playedCard = {playedCard.CardId}; usedChoice = {usedChoice?.CardId ?? "N/A"}");
            if (usedChoice?.CardId == null)
            {
                game.AddAsyncUpgradeCardUseInfo(new Model.AsyncUpgradeCardUseInfo
                {
                    CardName = playedCard.CardId,
                    RobotName = connections.GetRobot(connection).Name
                });
            }
            else
            {
                game.AddAsyncUpgradeCardUseInfo(new Model.AsyncChoiceUpgradeCardUseInfo
                {
                    CardName = playedCard.CardId,
                    ChosenChoice = new UpgradeCardChoice { ChoiceCardName = usedChoice.CardId },
                    RobotName = connections.GetRobot(connection).Name
                });
            }            
            log.Debug("PlayAsyncCard - message successfully enqueued");
        }
    }
}