﻿using RoboRoboRally.Server.Core.Lobbies;
using RoboRoboRally.Server.Interfaces;
using System;

namespace RoboRoboRally.Server.Core
{
    public interface IConnection
    {
        string ClientName { get; set; }
        Guid ConnectionId { get; set; }

        bool CanPlay { get; }

        bool SupportsClient<T>() where T : class, IClient;

        T GetClient<T>() where T : class, IClient;

        void SetSupportedTypes(Type[] types);

        void Close();

        void SendMessageToAllClients(Action<IClient> message);

        ILobby CreateLobbyListener(IServerLobby lobby);

        IOwnedLobby CreateOwnedLobbyListener(IServerLobby lobby);

        IGame CreateGameListener(IServerGame game);

        void RemovedFromLobby();
    }
}