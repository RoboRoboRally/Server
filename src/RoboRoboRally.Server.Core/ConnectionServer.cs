﻿using Common.Logging;
using RoboRoboRally.Communications;
using RoboRoboRally.Server.Core.Listeners;
using RoboRoboRally.Server.Core.Lobbies;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace RoboRoboRally.Server.Core
{
    public class ConnectionServer : IConnectionServer
    {
        private IRpcServer rpcServer;
        private CancellationTokenSource cts;
        private volatile bool Started = false;
        private Task serverTask;
        private IConfig config;
        private LobbyManager lobbyManager;
        private ILog log = LogManager.GetLogger<ConnectionServer>();

        static ConnectionServer()
        {
            AppDomain.CurrentDomain.Load("RoboRoboRally.Server.Interfaces");
            Logging.Setup();
        }

        public ConnectionServer(int port, bool useHttps, IConfig config)
        {
            this.config = config;
            rpcServer = new RpcServer(port, useHttps);
            cts = new CancellationTokenSource();
            lobbyManager = new LobbyManager(config);
        }

        public void Start()
        {
            if (Started)
                throw new InvalidOperationException("Server already started");
            serverTask = StartImpl();
            log.Info("Server started");
        }

        public IServer AddLocalConnection(IConnection connection)
        {
            log.Info("New local connection added");
            return new ServerListener(connection, lobbyManager);
        }

        private async Task StartImpl()
        {
            Started = true;
            while (Started && !cts.IsCancellationRequested)
            {
                await rpcServer.AcceptAsync(cts.Token, conn =>
                {
                    log.Info("New RPC connection opened");
                    var c = new Connections.RpcConnection(conn, lobbyManager);
                    conn.OnClose += (_, __) =>
                    {
                        log.Info("Rpc connection closed");
                    };
                });
            }
        }

        public void Close()
        {
            Started = false;
            cts.Cancel();
            serverTask.Wait();
			lobbyManager.CloseAllLobies();
            log.Info("Server closed");
        }
    }
}
