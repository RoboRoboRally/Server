﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;

namespace RoboRoboRally.Server.Core.Settings
{
    [ExcludeFromCodeCoverage]
    internal class ModelCardLoader : ICardLoader
    {
        CardLoader cardLoader;

        public ModelCardLoader(IConfig config)
        {
            cardLoader = new CardLoader($"{config.AbsoluteGameDataFolder}/DB/CardDB");
        }

        DamageCard ICardLoader.LoadDamageCard(string cardName) => cardLoader.LoadDamageCard(cardName);

        ProgrammingCard[] ICardLoader.LoadPlayerProgrammingCards() => cardLoader.LoadPlayerProgrammingCards();

        UpgradeCard[] ICardLoader.LoadUpgradeCards() => cardLoader.LoadUpgradeCards();

        IEnumerable<UpgradeCard> ICardLoader.LoadUpgradeCards(IEnumerable<string> cardNames)
        {
            foreach (var name in cardNames)
                yield return cardLoader.LoadUpgradeCard(name);
        }
    }
}
