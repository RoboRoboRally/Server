﻿using Common.Logging;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using ModelGameSettings = RoboRoboRally.Model.Settings.GameSettings;
using ModelMapSettings = RoboRoboRally.Model.Settings.MapSettings;

namespace RoboRoboRally.Server.Core.Settings
{
    internal class SettingsLoader
    {
        private ILog log = LogManager.GetLogger<SettingsLoader>();

        public ModelGameSettings GetSettings(IConfig config, GameSettings settings)
            => GetSettings(new ModelCardLoader(config), settings);        

        public ModelGameSettings GetSettings(ICardLoader cardLoader, GameSettings settings)
        {
            var set = new ModelGameSettings
            {
                CardSettings = GetCardSettings(cardLoader),
                MapSettings = GetMapSettings(settings.MapSettings),
                PlayerSettings = GetPlayerSettings(settings, cardLoader),
                EnvironmentSettings = GetEnvironmentSettings(cardLoader),
                GameFlowSettings = GetGameFlowSettings(settings),
                Seed = (int)(settings.Seed ?? DateTime.Now.Ticks)
            };

            return set;
        }
        
        private Model.Settings.EnvironmentSettings GetEnvironmentSettings(ICardLoader cardLoader)
        {
            return new Model.Settings.EnvironmentSettings()
            {
                DefaultRebootDamage = new Model.Util.Damage() { DamageCards = new[] { cardLoader.LoadDamageCard("Spam") } }
            };
        }

        private Model.Settings.GameFlowSettings GetGameFlowSettings(GameSettings settings)
        {
            var phases = new List<PhaseBase>();
            if (settings.IncludeUpgrades)
                phases.Add(new UpgradePhase());

            phases.Add(new ProgrammingPhase(9));
            phases.Add(new ActivationPhase());

            return new Model.Settings.GameFlowSettings()
            {
                Phases = phases.ToArray()
            };
        }

        private Model.Settings.PlayerSettings GetPlayerSettings(GameSettings settings, ICardLoader cardLoader)
        {
            bool programmingPackSet = settings.ProgrammingPack != null && settings.ProgrammingPack.Count != 0;
            int defaultCardCount = programmingPackSet ? 0 : 2;
            Dictionary<ProgrammingCard, int> cards = new Dictionary<ProgrammingCard, int>();
            foreach (var card in cardLoader.LoadPlayerProgrammingCards())
                cards.Add(card, defaultCardCount);

            if (!settings.IncludeUpgrades)
            {
                var powerup = cards.SingleOrDefault(c => c.Key.Name == "PowerUp").Key;
                if (powerup != null)
                    cards.Remove(powerup);
            }

            if (programmingPackSet)
            {
                log.Info("Programming pack is set, rewriting default card counts");
                foreach (var (cardInfo, count) in settings.ProgrammingPack)
                {
                    var card = cards.SingleOrDefault(c => c.Key.Name == cardInfo.CardId).Key;
                    if (card == null)
                        continue;
                    else
                        cards[card] = count;
                }
            }            

            return new Model.Settings.PlayerSettings
            {
                PlayerProgrammingCards = new Pack<ProgrammingCard>(cards.SelectMany(p => Enumerable.Repeat(p.Key, p.Value)).ToArray()),
                EnergyAmount = 5,
                RegisterBoardSize = 5,
                UpgradeCardsLimits = new Dictionary<UpgradeCardType, int>() { { UpgradeCardType.Permanent, 3 }, { UpgradeCardType.Temporary, 3 } },
                DefaultPlayerDamage = new Model.Util.Damage() { DamageCards = new[] { cardLoader.LoadDamageCard("Spam") } }
            };
        }

        private ModelMapSettings GetMapSettings(MapSettings mapSettings)
        {
            List<TileRecord<Model.MapDB.Tiles.CheckpointTile>> checkpoints = new List<TileRecord<Model.MapDB.Tiles.CheckpointTile>>();
            List<TileRecord<Model.MapDB.Tiles.RebootTile>> rebootTiles = new List<TileRecord<Model.MapDB.Tiles.RebootTile>>();

            for (int i = 0; i < mapSettings.CheckpointTiles.Length; ++i)
            {
                checkpoints.Add(new TileRecord<Model.MapDB.Tiles.CheckpointTile>
                {
                    Coords = mapSettings.CheckpointTiles[i],
                    Tile = new Model.MapDB.Tiles.CheckpointTile(i + 1)
                });
            }

            foreach (var (coords, direction) in mapSettings.RebootTiles)
            {
                rebootTiles.Add(new TileRecord<Model.MapDB.Tiles.RebootTile>
                {
                    Coords = coords,
                    Tile = new Model.MapDB.Tiles.RebootTile(direction)
                });
            }

            return new ModelMapSettings
            {
                GamePlanID = mapSettings.GamePlanId,
                StartingPlanID = mapSettings.StartingPlanId,
                MapModifications = new MapModifications
                {
                    GamePlanRotation = mapSettings.GamePlanRotation,
                    PriorityAntenna = new TileRecord<Model.MapDB.Tiles.PriorityAntennaTile>
                    {
                        Coords = mapSettings.PriorityAntenna.Coordinates,
                        Tile = new Model.MapDB.Tiles.PriorityAntennaTile(mapSettings.PriorityAntenna.Direction)
                    },
                    CheckpointTiles = checkpoints.ToArray(),
                    RebootTiles = rebootTiles.ToArray()
                }
            };
        }

        private Model.Settings.CardSettings GetCardSettings(ICardLoader cardLoader)
        {
            return new Model.Settings.CardSettings
            {
                UpgradeCards = new Pack<UpgradeCard>(cardLoader.LoadUpgradeCards())
            };
        }
    }
}