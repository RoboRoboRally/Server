﻿using RoboRoboRally.Model.CardDB.Cards;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Server.Core.Settings
{
    internal interface ICardLoader
    {
        DamageCard LoadDamageCard(string cardName);
        ProgrammingCard[] LoadPlayerProgrammingCards();
        UpgradeCard[] LoadUpgradeCards();
        IEnumerable<UpgradeCard> LoadUpgradeCards(IEnumerable<string> cardNames);
    }
}
