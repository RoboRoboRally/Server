﻿using RoboRoboRally.Server.Core;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace RoboRoboRally.Server
{
    internal class Config : IConfig
    {
        public string AbsoluteGameDataFolder => AppDomain.CurrentDomain.BaseDirectory + "/" + ConfigurationManager.AppSettings["GameDataFolder"];

        public IEnumerable<string> PluginFolders => ConfigurationManager.AppSettings["PluginFolders"].Split(';');

        public TimeSpan ProgrammingTimeout => TimeSpan.FromSeconds(int.Parse(ConfigurationManager.AppSettings["ProgrammingTimeoutInSeconds"]));

        public TimeSpan NetworkLatencyAcceptedDelay => TimeSpan
            .FromMilliseconds(int.Parse(ConfigurationManager.AppSettings["NetworkLatencyAcceptedDelayInMilliseconds"]));

        public TimeSpan UpgradeCardBuyTimeout => TimeSpan.FromSeconds(int.Parse(ConfigurationManager.AppSettings["UpgradeCardBuyTimeoutInSeconds"]));

        public TimeSpan UpgradeCardUseTimeout => TimeSpan.FromSeconds(int.Parse(ConfigurationManager.AppSettings["UpgradeCardUseTimeoutInSeconds"]));

        public TimeSpan AsyncCardOfferProcessTimeout => TimeSpan.FromMilliseconds(int.Parse(ConfigurationManager.AppSettings["AsyncCardOfferProcessTimeoutInMilliseconds"]));
    }
}