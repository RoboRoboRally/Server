﻿using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Server.Core.Connections;
using RoboRoboRally.Server.Interfaces;
using System;
using System.IO;
using System.Linq;
using System.Threading;

namespace RoboRoboRally.Server.LocalConnections
{
    internal class LocalSpectator : BaseConnection,
        ILobbySpectator,
        IGameFlowSpectator,
        IMovementSpectator,
        IProgrammingCardSpectator,
        IMapElementSpectator,
        IErrorSpectator
    {
        private TextWriter output;

        public LocalSpectator(TextWriter output)
        {
            this.output = output;
            ClientName = "Server File Spectator";
        }

        private void WriteMessage(string message)
        {
            string time = DateTime.Now.TimeOfDay.ToString("hh\\:mm\\:ss\\.fff");
            output.WriteLine($"{time} {message}");
            output.Flush();
            Thread.Sleep(100);
        }

        #region IConnection

        public override T GetClient<T>()
        {
            return this as T;
        }

        #endregion IConnection

        void IClient.GameStarted(IGame game)
        {
            WriteMessage($"Lobby - Game started");
        }

        #region ILobbySpectator

        void ILobbySpectator.RobotPicked(string clientName, RobotInfo robot)
        {
            WriteMessage($"Client {clientName} picked robot {robot.Name}.");
        }

        void ILobbySpectator.RobotFreed(string clientName, RobotInfo robot)
        {
            WriteMessage($"Client {clientName} no longer using robot {robot.Name}.");
        }

        void ILobbySpectator.ClientJoined(string clientName)
        {
            WriteMessage($"Client {clientName} joined the lobby.");
        }

        void ILobbySpectator.ClientLeft(string clientName)
        {
            WriteMessage($"Client {clientName} left the lobby.");
        }

        #endregion ILobbySpectator

        #region IGameFlowSpectator

        void IGameFlowSpectator.TurnStarted(int turnNumber)
        {
            WriteMessage($"Turn {turnNumber} started");
        }

        void IGameFlowSpectator.TurnEnded(int turnNumber)
        {
            WriteMessage($"Turn {turnNumber} ended");
        }

        void IGameFlowSpectator.PhaseStarted(string phaseName)
        {
            WriteMessage($"Phase {phaseName} started");
        }

        void IGameFlowSpectator.PhaseEnded(string phaseName)
        {
            WriteMessage($"Phase {phaseName} ended");
        }

        void IGameFlowSpectator.GamePaused()
        {
            WriteMessage("Game Paused");
        }

        void IGameFlowSpectator.GameResumed()
        {
            WriteMessage("Game Resumed");
        }

        void IGameFlowSpectator.GameOver()
        {
            WriteMessage("Game Over");
        }

        void IGameFlowSpectator.RobotFinishedGame(RobotInfo robot)
        {
            WriteMessage($"Robot {robot.Name} finished the game");
        }

        public void RobotVisitedCheckpoint(RobotInfo robot, int checkpointNr)
        {
            WriteMessage($"Robot {robot.Name} visited checkpoint number {checkpointNr}");
        }

        public void RobotPriorityDetermined(RobotInfo[] robotsInOrder)
        {
            WriteMessage($"Robots will activate their registers in this order: {string.Join(',', robotsInOrder.Select(r => r.Name))}");
        }

        #endregion IGameFlowSpectator

        #region IMovementSpectator

        void IMovementSpectator.RobotMoved(RobotInfo robot, MapCoordinates newCoords)
        {
            WriteMessage($"Robot {robot.Name} moved to [{newCoords.ColIndex}:{newCoords.RowIndex}]");
        }

        void IMovementSpectator.RobotTeleported(RobotInfo robot, MapCoordinates newCoords, Direction direction)
        {
            WriteMessage($"Robot {robot.Name} teleported to [{newCoords.ColIndex}:{newCoords.RowIndex}] facing [{direction.X}:{direction.Y}]");
        }

        void IMovementSpectator.RobotRotated(RobotInfo robot, RotationType rotation)
        {
            WriteMessage($"Robot {robot.Name} rotated {rotation}");
        }

        void IMovementSpectator.RobotAdded(RobotInfo robot, MapCoordinates coordinates, Direction direction)
        {
            WriteMessage($"Robot {robot.Name} added at [{coordinates.ColIndex}:{coordinates.RowIndex}] facing [{direction.X}:{direction.Y}]");
        }

        void IMovementSpectator.RobotRemoved(RobotInfo robot)
        {
            WriteMessage($"Robot {robot.Name} removed");
        }

        #endregion IMovementSpectator

        #region IProgrammingCardSpectator

        void IProgrammingCardSpectator.CardsDealt()
        {
            WriteMessage($"Cards were dealt");
        }

        void IProgrammingCardSpectator.CardsSelected(RobotInfo robot, int count)
        {
            WriteMessage($"Robot {robot.Name} selected {count} cards");
        }

        void IProgrammingCardSpectator.CardSelectionOver()
        {
            WriteMessage($"Cards were selected by all players");
        }

        void IProgrammingCardSpectator.RegisterActivated(int register)
        {
            WriteMessage($"Register {register} activated");
        }

        void IProgrammingCardSpectator.CardRevealed(RevealedProgrammingCard revealedCard)
        {
            WriteMessage($"Revealed card {revealedCard.RevealedCard} at register {revealedCard.Register} for robot {revealedCard.Robot.Name}");
        }

        void IProgrammingCardSpectator.CardBeingPlayed(RobotInfo robot, string card)
        {
            WriteMessage($"Card {card} of robot {robot.Name} is being played");
        }

        void IProgrammingCardSpectator.DamageCardsTaken(RobotInfo robot, Interfaces.Damage damage)
        {
            WriteMessage($"Robot {robot.Name} took damage, drawn cards: {string.Join(", ", damage.DamageCards)}");
        }

        void IProgrammingCardSpectator.RevealedCardReplaced(RobotInfo robot, int register, CardInfo oldCard, CardInfo newCard)
        {
            WriteMessage($"Robot {robot.Name} replaced his old card {oldCard.CardId} in register {register} with {newCard.CardId}");
        }

        void IProgrammingCardSpectator.CardsNotSelectedOnTime(RobotInfo robot)
        {
            WriteMessage($"Robot {robot.Name} did not select his cards on time");
        }

        #endregion IProgrammingCardSpectator

        #region IMapElementSpectator

        void IMapElementSpectator.MapElementActivationStarted()
        {
            WriteMessage("Map element activation started");
        }

        void IMapElementSpectator.BlueBeltsActivated()
        {
            WriteMessage("Blue belts activated");
        }

        void IMapElementSpectator.CheckpointsActivated()
        {
            WriteMessage("Checkpoints activated");
        }

        void IMapElementSpectator.EnergySpacesActivated()
        {
            WriteMessage("Energy spaces activated");
        }

        void IMapElementSpectator.GreenBeltsActivated()
        {
            WriteMessage("Green belts activated");
        }

        void IMapElementSpectator.PushPanelsActivated()
        {
            WriteMessage("Push panels activated");
        }

        void IMapElementSpectator.RotatingGearsActivated()
        {
            WriteMessage("Rotating gears activated");
        }

        void IMapElementSpectator.WallLasersActivated()
        {
            WriteMessage("Wall lasers activated");
        }

        #endregion IMapElementSpectator

        #region IErrorSpectator

        void IErrorSpectator.FatalRunningGameError(string message)
        {
            WriteMessage($"Fatal running game error: {message}");
        }

        void IErrorSpectator.FatalUnexpectedError(string message)
        {
            WriteMessage($"Fatal unexpected error: {message}");
        }

        void IMovementSpectator.RobotRebooting(RobotInfo robot, MapCoordinates mapCoordinates, Direction direction)
        {
            WriteMessage($"Robot {robot.Name} is rebooting to [{mapCoordinates.ColIndex}:{mapCoordinates.RowIndex}] facing [{direction.X}:{direction.Y}]");
        }

        public void LobbyClosing()
        {
            WriteMessage($"Lobby is closing");
        }

        public void LobbyClosed()
        {
            WriteMessage($"Lobby closed");
        }

        public void GameIsEnding()
        {
            WriteMessage($"Game is ending");
        }

        public void RobotPushed(RobotInfo robot, MapCoordinates oldCoords, MapCoordinates newCoords)
        {
            WriteMessage($"Robot {robot.Name} got pushed from {Coords(oldCoords)} to {Coords(newCoords)}");
        }

        public void CardBurned(RobotInfo robot, string card)
        {
            WriteMessage($"Robot {robot.Name} burned a card: {card}");
        }

        #endregion IErrorSpectator

        private string Coords(MapCoordinates coords)
        {
            return $"[{coords.ColIndex}:{coords.RowIndex}]";
        }
    }
}