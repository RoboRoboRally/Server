﻿using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Server.Core;
using RoboRoboRally.Server.Core.Connections;
using RoboRoboRally.Server.Interfaces;
using RoboRoboRally.Server.LocalConnections;
using System;
using System.IO;
using System.Linq;

namespace RoboRoboRally.Server
{
    internal class ConsoleCommandProcessor
    {
        private readonly TextReader input = Console.In;
        private readonly TextWriter output = Console.Out;
        private IConnectionServer server;

        public ConsoleCommandProcessor(IConnectionServer server)
        {
            this.server = server;
        }

        public void ProcessCommands()
        {
            while (true)
            {
                try
                {
                    string line = GetNextCommand();
                    var args = ParseCommand(line);

                    switch (args[0].ToLower())
                    {
                        case "lobby":
                            ProcessLobbyCommands(args);
                            break;

                        case "help":
                            PrintHelp();
                            break;

                        case "exit":
                            return;

                        default:
                            output.WriteLine("Unknown command. Write \"Help\" to get the list of available commands.");
                            break;
                    }
                }
                catch (Exception e)
                {
                    output.WriteLine($"An exception occured, message: {e.Message}");
                }
            }
        }

        private string GetNextCommand()
        {
            output.Write(">");
            return input.ReadLine();
        }

        private void PrintHelp()
        {
            output.WriteLine($"Available commands: ");
            output.WriteLine($"Lobby $(LobbyName)  - creates lobby named LobbyName");
            output.WriteLine($"help - prints this message");
        }

        private void ProcessLobbyCommands(string[] args)
        {
            if (args.Length != 2)
            {
                output.WriteLine("Missing lobby name");
                return;
            }

            string lobbyName = args[1];
            var connection = new LocalConnection();
            IServer srvr = server.AddLocalConnection(connection);
            IOwnedLobby lobby = srvr.CreateLobby(lobbyName);

            IGame game = null;

            output.WriteLine($"Created lobby {lobbyName}");

            while (true)
            {
                string command = GetNextCommand();
                args = ParseCommand(command);
                switch (args[0].ToLower())
                {
                    case "spectate":
                        ProcessSpectate(args, lobbyName);
                        break;

                    case "mockplay":
                        ProcessMockplay(args, lobby, lobbyName);
                        break;

                    case "kickRobot":
                        ProcessKickRobot(args, lobby);
                        break;

                    case "start":
                        ProcessStart(args, lobby, connection, out game);
                        break;

                    case "pause":
                        ProcessPause(game, connection);
                        break;

                    case "resume":
                        ProcessResume(game, connection);
                        break;

                    case "stop":
                        ProcessStop(game, connection);
                        break;

                    case "listplugins":
                        ListPlugins(lobby);
                        break;

                    case "addplugin":
                        AddPlugin(lobby, args);
                        break;

                    case "help":
                        PrintHelp_Lobby();
                        break;

                    case "exit":
                        output.WriteLine($"Exitting lobby");
                        goto Exit;
                    default:
                        output.WriteLine("Unknown command, write \"help\" to get the list of available commands");
                        break;
                }
            }
            Exit:
            lobby.LeaveLobby();
        }

        private void ProcessKickRobot(string[] args, IOwnedLobby lobby)
        {
            if (args.Length < 2)
            {
                output.WriteLine("Missing the name of the robot");
                return;
            }

            string robotName = args[1];
            var connections = lobby.GetAllConnections();
        }

        private void AddPlugin(IOwnedLobby lobby, string[] args)
        {
            if (args.Length < 2)
            {
                output.WriteLine("Missing plugin nr.");
                return;
            }
            if (!int.TryParse(args[1], out int pluginNr))
            {
                output.WriteLine($"Can not parse \"{pluginNr}\" as an integer");
                return;
            }

            var plugins = lobby.GetAvailablePlugins();
            if (pluginNr <= 0 || plugins.Length > pluginNr)
            {
                output.WriteLine($"plugin number out of range");
                return;
            }
            var plugin = plugins[pluginNr - 1];
            lobby.AddPlugin(plugin);

            output.WriteLine($"Added plugin {plugin.Name} v{plugin.Version}");
        }

        private void ListPlugins(ILobby lobby)
        {
            var plugins = lobby.GetAvailablePlugins();
            if (plugins.Length == 0)
            {
                output.WriteLine("No plugins available");
                return;
            }
            output.WriteLine("Listing available plugins:");
            for (int i = 0; i < plugins.Length; ++i)
            {
                var plugin = plugins[i];
                output.WriteLine($"{i + 1}: '{plugin.Name}' v{plugin.Version} - {plugin.Description}");
                output.WriteLine();
            }
        }

        private void ProcessStop(IGame game, LocalConnection connection)
        {
            if (game == null)
            {
                output.WriteLine($"No game to stop");
            }
            else
            {
                game.StopGame();
                output.WriteLine($"Game stopped");
            }
        }

        private void ProcessResume(IGame game, LocalConnection connection)
        {
            if (game == null)
            {
                output.WriteLine($"No game to resume");
            }
            else
            {
                game.Resume();
                output.WriteLine($"Game resumed");
            }
        }

        private void ProcessPause(IGame game, IConnection connection)
        {
            if (game == null)
            {
                output.WriteLine($"No game to pause");
            }
            else
            {
                game.Pause();
                output.WriteLine($"Game paused");
            }
        }

        private void ProcessStart(string[] args, ILobby lobby, LocalConnection connection, out IGame game)
        {
            long? seed = null;
            if (args.Length == 2)
            {
                if (long.TryParse(args[1], out long s))
                {
                    seed = s;
                }
                else
                {
                    output.WriteLine($"Cannot parse {args[1]} as long.");
                    game = null;
                    return;
                }
            }

            GameSettings settings = new GameSettings
            {
                MapSettings = new MapSettings
                {
                    GamePlanId = "5B",
                    StartingPlanId = "A",
                    GamePlanRotation = RotationType.None,
                    PriorityAntenna = new OrientedMapCoordinates
                    {
                        Coordinates = new MapCoordinates(0, 5),
                        Direction = new Direction(DirectionType.Right)
                    },
                    RebootTiles = new[]
                    {
                        new OrientedMapCoordinates
                        {
                            Coordinates = new MapCoordinates(8,9),
                            Direction = new Direction(DirectionType.Up)
                        }
                    },
                    CheckpointTiles = new[]
                    {
                        new MapCoordinates(0, 4)
                    }
                },
                Seed = seed
            };

            lobby.StartGame(settings);
            game = connection.GetGame();
            output.WriteLine($"Lobby started");
        }

        private void ProcessMockplay(string[] args, IOwnedLobby lobby, string lobbyName)
        {
            var plugin = lobby.GetAvailablePlugins().Where(p => p.Name == "AI - take first").FirstOrDefault();
            if (plugin == null)
            {
                Console.WriteLine("Mock player plugin not found");
                return;
            }
            lobby.AddPlugin(plugin);
            output.WriteLine($"Added mock player to lobby {lobbyName}");
        }

        private void ProcessSpectate(string[] args, string lobbyName)
        {
            if (args.Length < 2)
            {
                output.WriteLine("Missing output file name");
                return;
            }
            var fileName = args[1];
            AddConnectionToLobby(new LocalSpectator(new StreamWriter(fileName)), lobbyName);
            output.WriteLine($"Spectating Lobby {lobbyName}, generating output into {fileName}");
        }

        private void PrintHelp_Lobby()
        {
            output.WriteLine($"Available commands: ");
            output.WriteLine($"Spectate $(outputFileName) - spectates current lobby, sending output to specified file.");
            output.WriteLine($"MockPlay - adds mock player to current lobby");
            output.WriteLine($"Start [$seed] - starts the game in current lobby, optionally accepts seed parameter");
            output.WriteLine($"Pause - pauses the game in current lobby");
            output.WriteLine($"Resume - resumes the game in current lobby");
            output.WriteLine($"Stop - stops the game in current lobby");
            output.WriteLine($"ListPlugins - Lists all available plugins");
            output.WriteLine($"AddPlugin $(pluginNr) - Adds the plugin with the specified pluginNr to the lobby. The PluginNr matches to the one outputted by the ListPlugins command");
            output.WriteLine($"Help - prints this help");
            output.WriteLine($"Exit - leaves the lobby");
        }

        private ILobby AddConnectionToLobby(IConnection spectator, string lobbyName)
        {
            var s = server.AddLocalConnection(spectator);
            s.Connect(spectator.ClientName, spectator.GetType().GetInterfaces());
            var lobby = s.JoinLobby(lobbyName);
            return lobby;
        }

        private string[] ParseCommand(string command)
        {
            return command.Split(' ');
        }
    }
}