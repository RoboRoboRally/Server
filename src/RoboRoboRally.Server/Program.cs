﻿using Common.Logging;
using RoboRoboRally.Server.Core;
using System;
using System.Reflection;

namespace RoboRoboRally.Server
{
    internal class Program
    {
        private const ushort DEFAULT_PORT = 54320;

        public static void Main(string[] args)
        {
            SetupLogging();

            if (!ProcessArgs(args, out ushort port))
                return;

            ConnectionServer connectionServer = new ConnectionServer(port, useHttps: false, config: new Config());
            connectionServer.Start();
            Console.WriteLine($"Started server on port {port}");

            var processor = new ConsoleCommandProcessor(connectionServer);
            processor.ProcessCommands();

            connectionServer.Close();
        }

        private static void SetupLogging()
        {
            Logging.Setup();
            ILog log = LogManager.GetLogger<Program>();
        }

        private static bool ProcessArgs(string[] args, out ushort port)
        {
            port = DEFAULT_PORT;

            for (int i = 0; i < args.Length; ++i)
            {
                switch (args[i])
                {
                    case "-h":
                        PrintHelp();
                        return false;

                    case "-p":
                        if (i + 1 < args.Length)
                        {
                            if (!ushort.TryParse(args[i + 1], out port))
                            {
                                Console.WriteLine($"Error parsing port: {args[i + 1]}");
                                return false;
                            }
                            ++i;
                        }
                        else
                        {
                            Console.WriteLine("Missing port number");
                            return false;
                        }
                        break;
                }
            }

            return true;
        }

        public static void PrintHelp()
        {
            Console.WriteLine(
$@"RoboRoboRally server version {Assembly.GetExecutingAssembly().GetName().Version.ToString()}

Usage: server [-p port] | [-h]

Options:
    -p sets port, default port: {DEFAULT_PORT}
    -h prints this help
");
        }
    }
}