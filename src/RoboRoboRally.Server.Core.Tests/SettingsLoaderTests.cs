using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Server.Core;
using RoboRoboRally.Server.Core.Settings;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.Generic;

namespace RoboRoboRally.Server.Core.Tests
{
    [TestFixture]
    public class SettingsLoaderTests
    {
        private MockRepository mockRepository;
        private Mock<ICardLoader> cardLoader;


        [SetUp]
        public void SetUp()
        {
            this.mockRepository = new MockRepository(MockBehavior.Loose);
            cardLoader = mockRepository.Create<ICardLoader>();            
        }

        [TearDown]
        public void TearDown()
        {
            this.mockRepository.VerifyAll();
        }

        private SettingsLoader CreateSettingsLoader()
        {
            return new SettingsLoader();
        }

        private GameSettings CreateEmptySettings()
        {
            return new GameSettings
            {
                MapSettings = new MapSettings
                {
                    CheckpointTiles = Array.Empty<MapCoordinates>(),
                    GamePlanId = string.Empty,
                    StartingPlanId = string.Empty,
                    GamePlanRotation = RotationType.None,
                    PriorityAntenna = new OrientedMapCoordinates { Coordinates = new MapCoordinates(), Direction = new Direction(DirectionType.Up) },
                    RebootTiles = Array.Empty<OrientedMapCoordinates>()
                },
                IncludeUpgrades = false
            };
        }

        [Test]
        public void GetSettings_CardSettings_LoadsUpgradeCards()
        {
            var settingsLoader = CreateSettingsLoader();
            GameSettings settings = CreateEmptySettings();
            var expected = new UpgradeCard[2];
            cardLoader.Setup(c => c.LoadUpgradeCards()).Returns(expected);

            var result = settingsLoader.GetSettings(cardLoader.Object, settings);

            var actualPack = result.CardSettings.UpgradeCards;
            var cards = actualPack.Draw(2);
            Assert.AreEqual(0, actualPack.Size());
            CollectionAssert.AreEquivalent(expected, cards);
        }

        [Test]
        public void GetSettings_PlayerSettings_Defaults()
        {
            var settingsLoader = CreateSettingsLoader();
            var settings = CreateEmptySettings();
            var damageCard = new DamageCard("foo", null);
            var programmingCards = new[] { new ProgrammingCard("foo", null) };
            cardLoader.Setup(c => c.LoadPlayerProgrammingCards()).Returns(programmingCards);
            cardLoader.Setup(c => c.LoadDamageCard(It.IsAny<string>())).Returns(damageCard);

            var result = settingsLoader.GetSettings(cardLoader.Object, settings);

            var playerSettings = result.PlayerSettings;
            Assert.AreEqual(5, playerSettings.EnergyAmount);
            Assert.AreEqual(5, playerSettings.RegisterBoardSize);
            CollectionAssert.AreEquivalent(new[] { damageCard }, playerSettings.DefaultPlayerDamage.DamageCards);

            var actualCards = playerSettings.PlayerProgrammingCards.Draw(2);
            Assert.AreEqual(0, playerSettings.PlayerProgrammingCards.Size());
            CollectionAssert.AreEquivalent(new[] { programmingCards[0], programmingCards[0] }, actualCards);

            Dictionary<UpgradeCardType, int> limits = new Dictionary<UpgradeCardType, int>
            {
                {UpgradeCardType.Permanent, 3 },
                {UpgradeCardType.Temporary, 3 }
            };
            CollectionAssert.AreEquivalent(limits, playerSettings.UpgradeCardsLimits);
        }

        [Test]
        public void GetSettings_GameFlowSettings_Defaults()
        {
            var loader = CreateSettingsLoader();
            var settings = CreateEmptySettings();

            var result = loader.GetSettings(cardLoader.Object, settings);

            Assert.AreEqual(2, result.GameFlowSettings.Phases.Length);
            Assert.AreEqual(typeof(ProgrammingPhase), result.GameFlowSettings.Phases[0].GetType());
            Assert.AreEqual(typeof(ActivationPhase), result.GameFlowSettings.Phases[1].GetType());
        }

        [Test]
        public void GetSettings_EnvironmentSettings_Defaults()
        {
            var loader = CreateSettingsLoader();
            var settings = CreateEmptySettings();
            var damage = new DamageCard("foo", null);
            cardLoader.Setup(l => l.LoadDamageCard(It.IsAny<string>())).Returns(damage);

            var result = loader.GetSettings(cardLoader.Object, settings);

            CollectionAssert.AreEquivalent(new[] { damage }, result.EnvironmentSettings.DefaultRebootDamage.DamageCards);
        }

        [Test]
        public void GetSettings_MapSettings_GamePlansCorrect()
        {
            const string expectedGamePlanId = "GamePlanId";
            const string expectedStartingPlanId = "StartingPlanId";
            const RotationType expectedGamePlanRotation = RotationType.Left;

            var loader = CreateSettingsLoader();
            var settings = CreateEmptySettings();
            settings.MapSettings.GamePlanId = expectedGamePlanId;
            settings.MapSettings.StartingPlanId = expectedStartingPlanId;
            settings.MapSettings.GamePlanRotation = expectedGamePlanRotation;

            var result = loader.GetSettings(cardLoader.Object, settings);

            Assert.AreEqual(expectedGamePlanId, result.MapSettings.GamePlanID);
            Assert.AreEqual(expectedStartingPlanId, result.MapSettings.StartingPlanID);
            Assert.AreEqual(expectedGamePlanRotation, result.MapSettings.MapModifications.GamePlanRotation);
        }

        [Test]
        public void GetSettings_MapSettings_SpecialTilesCorrect()
        {
            var loader = CreateSettingsLoader();
            var settings = CreateEmptySettings();
            var originalMapSettings = settings.MapSettings;
            originalMapSettings.PriorityAntenna = new OrientedMapCoordinates
            {
                Coordinates = new MapCoordinates(5, 5),
                Direction = new Direction(DirectionType.Left)
            };
            originalMapSettings.RebootTiles = new[]
            {
                new OrientedMapCoordinates
                {
                    Coordinates = new MapCoordinates(3, 6),
                    Direction = new Direction(DirectionType.Right)
                }
            };
            originalMapSettings.CheckpointTiles = new[]
            {
                new MapCoordinates(4, 7)
            };

            var result = loader.GetSettings(cardLoader.Object, settings);

            var actualModifications = result.MapSettings.MapModifications;
            Assert.AreEqual(originalMapSettings.PriorityAntenna.Coordinates, actualModifications.PriorityAntenna.Coords);
            Assert.AreEqual(originalMapSettings.PriorityAntenna.Direction, actualModifications.PriorityAntenna.Tile.OutDirection);

            Assert.AreEqual(originalMapSettings.RebootTiles.Length, actualModifications.RebootTiles.Length);
            Assert.AreEqual(originalMapSettings.RebootTiles[0].Coordinates, actualModifications.RebootTiles[0].Coords);
            Assert.AreEqual(originalMapSettings.RebootTiles[0].Direction, actualModifications.RebootTiles[0].Tile.OutDirection);

            Assert.AreEqual(originalMapSettings.CheckpointTiles.Length, actualModifications.CheckpointTiles.Length);
            Assert.AreEqual(originalMapSettings.CheckpointTiles[0], actualModifications.CheckpointTiles[0].Coords);
            Assert.AreEqual(1, actualModifications.CheckpointTiles[0].Tile.Number);
        }
    }
}
