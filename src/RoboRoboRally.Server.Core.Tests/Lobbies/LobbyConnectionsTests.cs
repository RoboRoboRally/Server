using Moq;
using NUnit.Framework;
using RoboRoboRally.Server.Core;
using RoboRoboRally.Server.Core.Connections;
using RoboRoboRally.Server.Core.Lobbies;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RoboRoboRally.Server.Core.Lobbies.Tests
{
    [TestFixture]
    public class LobbyConnectionsTests
    {
        private MockRepository mockRepository;

        [SetUp]
        public void SetUp()
        {
            this.mockRepository = new MockRepository(MockBehavior.Strict);
        }

        [TearDown]
        public void TearDown()
        {
            this.mockRepository.VerifyAll();
        }

        private LobbyConnections CreateLobbyConnections()
        {
            return new LobbyConnections();
        }

        private RobotInfo CreateRobot()
        {
            return new RobotInfo
            {
                Name = "Foo",
                Color = "Red",
                Order = 1
            };
            
        }

        [Test]
        public void SetupRobots_AddRobots_RobotBecomesAvailable()
        {   
            var unitUnderTest = CreateLobbyConnections();
            RobotInfo[] robots = new[] 
            {
                CreateRobot()
            };

            unitUnderTest.SetupRobots(robots);

            Assert.AreEqual(robots[0], unitUnderTest.GetRobot(robots[0].Name));
            CollectionAssert.AreEquivalent(new[] { new KeyValuePair<RobotInfo, IConnection>(robots[0], null) }, unitUnderTest.GetRobotConnections());
        }

        [Test]
        public void AddConnection_Empty_ConnectionAddedAndRecoverable()
        {
            var unitUnderTest = CreateLobbyConnections();
            IConnection connection = new LocalConnection();

            unitUnderTest.AddConnection(connection);

            Assert.IsTrue(unitUnderTest.GetConnections().Contains(connection));
        }

        [Test]
        public void RemoveConnection_ContainsConnection_ConnectionGetsRemoved()
        {
            var unitUnderTest = CreateLobbyConnections();
            IConnection connection = new LocalConnection();
            unitUnderTest.AddConnection(connection);

            unitUnderTest.RemoveConnection(connection);

            Assert.IsFalse(unitUnderTest.GetConnections().Contains(connection));
        }

        [Test]
        public void FreeRobot_RobotUsed_RobotFreed()
        {
            var unitUnderTest = CreateLobbyConnections();
            IConnection connection = new LocalConnection();
            unitUnderTest.AddConnection(connection);
            RobotInfo robot = CreateRobot();
            unitUnderTest.SetupRobots(new[] { robot });
            unitUnderTest.AssignRobot(connection, robot);

            unitUnderTest.FreeRobot(robot);

            Assert.IsNull(unitUnderTest.GetRobotConnections().Single(p => p.Key == robot).Value);            
        }

        [Test]
        public void SendMessage_ConnectionInLobby_MessageGetsSend()
        {
            var unitUnderTest = CreateLobbyConnections();
            Mock<IConnection> connection = mockRepository.Create<IConnection>();
            Mock<IClient> client = mockRepository.Create<IClient>();
            connection.Setup(c => c.SupportsClient<IClient>()).Returns(true);
            connection.Setup(c => c.GetClient<IClient>()).Returns(client.Object);
            unitUnderTest.AddConnection(connection.Object);

            unitUnderTest.SendMessage<IClient>(c => c.GameStarted(null));

            client.Verify(c => c.GameStarted(null), Times.Once);
        }   

        [Test]
        public void CheckConnectionInLobby_ConnectionIsInLobby_Returns()
        {
            var unitUnderTest = CreateLobbyConnections();
            IConnection connection = new LocalConnection();
            unitUnderTest.AddConnection(connection);
         
            unitUnderTest.CheckConnectionInLobby(connection);
        }

        [Test]
        public void CheckConnectionInLobby_ConnectionIsNotInLobby_Throws()
        {
            var unitUnderTest = CreateLobbyConnections();
            IConnection connection = new LocalConnection();

            Assert.Throws<InvalidOperationException>(() => unitUnderTest.CheckConnectionInLobby(connection));
        }

        [Test]
        public void AssignRobot_RobotAvailable_RobotGetsAssigned()
        {
            // Arrange
            var unitUnderTest = CreateLobbyConnections();
            IConnection connection = new LocalConnection();
            RobotInfo robot = CreateRobot();
            unitUnderTest.SetupRobots(new[] { robot });
            unitUnderTest.AddConnection(connection);

            unitUnderTest.AssignRobot(connection, robot);

            Assert.That(unitUnderTest.GetRobotConnections().Where(p => p.Key == robot && p.Value == connection).Any());
        }        

        [Test]
        public void AddPlugin_NewObject_PluginsGetsAdded()
        {
            var unitUnderTest = CreateLobbyConnections();
            IConnection pluginConnection = new LocalConnection();
            PluginInfo id = new PluginInfo
            {
                Name = "Test",
                Version = "1.0.0",
                Description = "a description"
            };

            unitUnderTest.AddPlugin(pluginConnection, id);

            var plugins = unitUnderTest.GetAddedPlugins();
            Assert.AreEqual(1, plugins.Length);
            Assert.AreEqual(pluginConnection.ConnectionId.ToString(), plugins[0].InstanceId);
            Assert.AreEqual(id, plugins[0].Plugin);
        }
    }
}
